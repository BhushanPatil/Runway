<?php
/**
 * Runway functions and definitions
 *
 * @package Runway
 */

/**
 * Theme functions
 */
require get_parent_theme_file_path( 'inc/theme-setup.php' );

/**
 * Theme Template functions
 */
require get_parent_theme_file_path( 'inc/template-tags.php' );
require get_parent_theme_file_path( 'inc/template-functions.php' );
require get_parent_theme_file_path( 'inc/class-runway-walker-nav-menu.php' );
require get_parent_theme_file_path( 'inc/class-runway-walker-comment.php' );

/**
 * Admin functions
 */
require get_parent_theme_file_path( 'inc/sanitization.php' );
require get_parent_theme_file_path( 'inc/customize.php' );

/**
 * Widgets
 */
require get_parent_theme_file_path( 'inc/widgets.php' );

/**
 * Shortcodes
 */
require get_parent_theme_file_path( 'inc/shortcodes.php' );

/**
 * Helper Functions
 */
require get_parent_theme_file_path( 'inc/helper-functions.php' );
