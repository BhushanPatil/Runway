<?php
/**
 * Template part for displaying Prev and Next link pagination.
 *
 * @package Runway
 */

$paged         = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$max_num_pages = $wp_query->max_num_pages;

/* translators: 1: Previous page arrow */
$prev_link = get_previous_posts_link( sprintf( esc_html__( '%s Newer', 'runway' ), '<i class="material-icons">&#xE5C4;</i>' ) );

/* translators: 1: Current page number, 2: Total pages number. */
$index = sprintf( esc_html__( 'Page %1$s of %2$s', 'runway' ), $paged, $max_num_pages );

/* translators: 1: Next page arrow */
$next_link = get_next_posts_link( sprintf( esc_html__( 'Older %s', 'runway' ), '<i class="material-icons">&#xE5C8;</i>' ) );

?>
<nav class="pagination">
	<?php if ( ! empty( trim( $prev_link ) ) ) : ?>
	<span class="pagination__prev"><?php echo $prev_link; // WPCS: XSS ok. ?></span>
	<?php endif; ?>
	<span class="pagination__index"><?php echo $index; // WPCS: XSS ok. ?></span>
	<?php if ( ! empty( trim( $next_link ) ) ) : ?>
	<span class="pagination__next"><?php echo $next_link; // WPCS: XSS ok. ?></span>
	<?php endif; ?>
</nav>
