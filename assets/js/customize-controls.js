/**
 * Scripts within the customizer controls window.
 */
( function( $ ) {
	wp.customize( 'header_bg_image', function( value ) {
		value.bind( function( to ) {
			wp.customize.control( 'header_bg_image_opacity' ).active.set( to ? true : false );
		});
	});
	wp.customize( 'hero_content', function( value ) {
		value.bind( function( to ) {
			var intro = 'intro' === to ? true : false,
				slider = 'slider' === to ? true : false;
			wp.customize.control( 'hero_intro_title' ).active.set( intro );
			wp.customize.control( 'hero_intro_copy' ).active.set( intro );
			wp.customize.control( 'hero_slider_style' ).active.set( slider );
			wp.customize.control( 'hero_slider_number_of_posts' ).active.set( slider );
			wp.customize.control( 'hero_slider_autoplay' ).active.set( slider );
			wp.customize.control( 'hero_slider_arrows' ).active.set( slider );
			wp.customize.control( 'hero_slider_dots' ).active.set( slider );
			wp.customize.control( 'hero_slider_fade' ).active.set( slider );
			wp.customize.control( 'hero_slider_infinite' ).active.set( slider );
		});
	});
	wp.customize( 'show_on_front', function( value ) {
		value.bind( function( to ) {
			var posts = ( 'posts' === to ? true : false ),
				heading = wp.customize.control( 'homepage_show_heading' ).setting.get(),
				showSidebar = wp.customize.control( 'homepage_show_sidebar' ).setting.get();
			wp.customize.control( 'homepage_full_width_first_post' ).active.set( posts );
			wp.customize.control( 'homepage_show_heading' ).active.set( posts );
			wp.customize.control( 'homepage_latest_posts_heading' ).active.set( ( posts && heading ? true : false ) );
			wp.customize.control( 'homepage_latest_posts_subheading' ).active.set( ( posts && heading ? true : false ) );
			wp.customize.control( 'homepage_show_sidebar' ).active.set( posts );
			wp.customize.control( 'homepage_sidebar_width' ).active.set( ( posts && showSidebar ? true : false ) );
			wp.customize.control( 'post_excerpt_length' ).active.set( posts );
		});
	});
	wp.customize( 'homepage_show_heading', function( value ) {
		value.bind( function( to ) {
			wp.customize.control( 'homepage_latest_posts_heading' ).active.set( to );
			wp.customize.control( 'homepage_latest_posts_subheading' ).active.set( to );
		});
	});
	wp.customize( 'homepage_show_sidebar', function( value ) {
		value.bind( function( to ) {
			wp.customize.control( 'homepage_sidebar_width' ).active.set( to );
		});
	});
	wp.customize( 'post_show_sidebar', function( value ) {
		value.bind( function( to ) {
			wp.customize.control( 'post_sidebar_width' ).active.set( to );
		});
	});
	wp.customize( 'social_sharing', function( value ) {
		value.bind( function( to ) {
			wp.customize.control( 'twitter_id' ).active.set( to.includes( 'twitter' ) );
		});
	});
	wp.customize( 'footer_bg_transparent', function( value ) {
		value.bind( function( to ) {
			wp.customize.control( 'footer_bg_color' ).active.set( to ? false : true );
			wp.customize.control( 'footer_full_width' ).active.set( to ? false : true );
		});
	});
	wp.customize( 'footer_columns', function( value ) {
		value.bind( function( to ) {
			wp.customize.control( 'footer_column_1_width' ).active.set( 1 <= to ? true : false );
			wp.customize.control( 'footer_column_2_width' ).active.set( 2 <= to ? true : false );
			wp.customize.control( 'footer_column_3_width' ).active.set( 3 <= to ? true : false );
			wp.customize.control( 'footer_column_4_width' ).active.set( 4 <= to ? true : false );
			wp.customize.control( 'footer_column_5_width' ).active.set( 5 <= to ? true : false );
			wp.customize.control( 'footer_column_6_width' ).active.set( 6 <= to ? true : false );
		});
	});
} ( jQuery ) );
