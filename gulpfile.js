var package			= require( './package.json' ),
	autoprefixer 	= require( 'autoprefixer' ),
	browserSync 	= require( 'browser-sync' ).create(),
	gulp 			= require( 'gulp' ),
	postCss 		= require( 'gulp-postcss' ),
	sass 			= require( 'gulp-sass' ),
	wait 			= require( 'gulp-wait' ),
	zip				= require( 'gulp-zip' );

var sassConfig = {
	outputStyle: 'expanded',
	precision: 10
};

var autoprefixerBrowsers = [
	'ie >= 9',
	'ie_mob >= 9',
	'ff >= 30',
	'chrome >= 34',
	'safari >= 7',
	'opera >= 23',
	'ios >= 7',
	'android >= 4.4',
	'bb >= 10'
];

var postcssPlugins = [
	autoprefixer( { browsers: autoprefixerBrowsers } )
];

var zipFiles = [
	'*',
	'./assets/**/*',
	'./inc/**/*',
	'./template-parts/**/*',
	'!designs',
	'!node_modules',
	'!sass',
	'!gulp-default.bat',
	'!gulpfile.js',
	'!package.json',
	'!package-lock.json',
	'!phpcs.xml.dist',
	'!' + package.name + '.zip'
];

var browserSyncServer = {
	proxy: 'localhost/wp',
	browser: 'firefox',
	open: true,
	notify: false
};

gulp.task( 'css', function() {
	return gulp.src( [ './sass/style.scss' ] )
		.pipe( wait( 100 ) )
		.pipe( sass( sassConfig ).on( 'error', sass.logError ) )
		.pipe( postCss( postcssPlugins ) )
		.pipe( gulp.dest( '.' ) );
} );

gulp.task( 'customizeCss', function() {
	return gulp.src( [ './sass/customize-custom-controls.scss' ] )
		.pipe( sass( sassConfig).on( 'error', sass.logError ) )
		.pipe( postCss( postcssPlugins ) )
		.pipe( gulp.dest( './assets/css' ) );
} );

gulp.task( 'zip', function() {
	return gulp.src( zipFiles, { base: '.' } )
		.pipe( zip( package.name + '.zip' ) )
		.pipe( gulp.dest( '.' ) );
} );

gulp.task( 'serve', function() {
	browserSync.init( browserSyncServer );
	gulp.watch( [ './sass/**/*.scss', '!./sass/customize-custom-controls.scss' ], [ 'css', browserSync.reload ] );
	gulp.watch( [ './sass/customize-custom-controls.scss' ], [ 'customizeCss', browserSync.reload ] );
	gulp.watch( [ './assets/**/*' ], browserSync.reload );
	gulp.watch( [ './**/*.php', '!node_modules' ], browserSync.reload );
} );

gulp.task( 'default', [ 'serve' ] );
