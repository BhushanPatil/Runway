<?php
/**
 * Additional helper PHP functions.
 *
 * @package Runway
 */

/**
 * Converts an HTML style HEX color code to an RGB encoded integer.
 *
 * @param array $hex HTML style HEX color code.
 * @return object
 */
function runway_html_to_rgb( $hex ) {

	if ( '#' === $hex[0] ) {
		$hex = substr( $hex, 1 );
	}

	if ( 3 === strlen( $hex ) ) {
		$hex = $hex[0] . $hex[0] . $hex[1] . $hex[1] . $hex[2] . $hex[2];
	}

	$r = hexdec( $hex[0] . $hex[1] );
	$g = hexdec( $hex[2] . $hex[3] );
	$b = hexdec( $hex[4] . $hex[5] );

	return $b + ( $g << 0x8 ) + ( $r << 0x10 );
}

/**
 * Converts an RGB encoded integer to an HSL encoded integer.
 *
 * @param array $rgb RGB encoded integer.
 * @return array
 */
function runway_rgb_to_hsl( $rgb ) {

	$r = 0xFF & ( $rgb >> 0x10 );
	$g = 0xFF & ( $rgb >> 0x8 );
	$b = 0xFF & $rgb;

	$r = ( (float) $r ) / 255.0;
	$g = ( (float) $g ) / 255.0;
	$b = ( (float) $b ) / 255.0;

	$max_c = max( $r, $g, $b );
	$min_c = min( $r, $g, $b );

	$l = ( $max_c + $min_c ) / 2.0;

	if ( $max_c === $min_c ) {
		$s = 0;
		$h = 0;
	} else {
		if ( $l < .5 ) {
			$s = ( $max_c - $min_c ) / ( $max_c + $min_c );
		} else {
			$s = ( $max_c - $min_c ) / ( 2.0 - $max_c - $min_c );
		}
		if ( $r === $max_c ) {
			$h = ( $g - $b ) / ( $max_c - $min_c );
		}
		if ( $g === $max_c ) {
			$h = 2.0 + ( $b - $r ) / ( $max_c - $min_c );
		}
		if ( $b === $max_c ) {
			$h = 4.0 + ( $r - $g ) / ( $max_c - $min_c );
		}
		$h = $h / 6.0;
	}

	$h = (int) round( 255.0 * $h );
	$s = (int) round( 255.0 * $s );
	$l = (int) round( 255.0 * $l );

	return (object) array(
		'hue'        => $h,
		'saturation' => $s,
		'lightness'  => $l,
	);
}
