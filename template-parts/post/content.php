<?php
/**
 * Template part for displaying posts
 *
 * @package Runway
 */

$entry_classes   = array();
$entry_classes[] = 'entry--title-' . get_theme_mod( 'post_title_align', 'center' );

$title_text_transform = get_theme_mod( 'title_text_transform', 'capitalize' );

$title_classes = 'entry__title';
if ( 'none' !== $title_text_transform ) {
	$title_classes .= ' text-transform-' . $title_text_transform;
}
?>
<article <?php runway_post_class( 'entry', $entry_classes ); ?>>
	<header class="entry__header">
		<div class="entry__wrapper">
			<h1 class="<?php echo esc_attr( $title_classes ); ?>">
				<span class="entry__title-text"><?php the_title(); ?></span>
			</h1>
		</div>
		<?php if ( has_post_thumbnail() ) : ?>
		<div class="entry__thumbnail">
			<?php
			the_post_thumbnail(
				array( 1080, 1080 ),
				array(
					'class' => 'entry__thumbnail-image',
				)
			);
			?>
		</div>
		<?php endif; ?>
	</header>
	<div class="entry__wrapper">
		<?php if ( ! empty( trim( get_the_content() ) ) ) : ?>
		<div class="entry__content"><?php the_content(); ?></div>
		<?php endif; ?>
		<footer class="entry__footer">
			<?php get_template_part( 'template-parts/metadata/tags' ); ?>
			<div class="entry__footer-metadata">
				<?php get_template_part( 'template-parts/metadata/single' ); ?>
			</div>
			<div class="entry__footer-social-share-links"><?php runway_social_sharing( get_theme_mod( 'social_sharing', array( 'facebook', 'twitter', 'googleplus' ) ) ); ?></div>
		</footer>
	</div>
</article>
