<?php
/**
 * The template for displaying the footer
 *
 * @package Runway
 */

$full_width           = get_theme_mod( 'footer_full_width', true );
$columns              = get_theme_mod( 'footer_columns', 3 );
$bg_color_transparent = get_theme_mod( 'footer_bg_transparent', false );
$bg_color             = get_theme_mod( 'footer_bg_color', '#212121' );
$text_colors          = get_theme_mod( 'footer_text_colors', 'auto' );

$classes = 'footer';
$style   = '';

if ( ! $full_width ) {
	$classes .= ' footer--narrow';
}
if ( ! $bg_color_transparent ) {
	$style .= 'background-color:' . $bg_color . ';';
	if ( 'auto' === $text_colors ) {
		$lightness = runway_rgb_to_hsl( runway_html_to_rgb( $bg_color ) )->lightness;
		if ( ( 255 / 2 ) > $lightness ) {
			$classes .= ' text-colors-light';
		}
	} else {
		$classes .= ' text-colors-' . $text_colors;
	}
}

?>
	<footer class="<?php echo esc_attr( $classes ); ?>" style="<?php echo esc_attr( $style ); ?>" role="contentinfo">
		<div class="footer__inner footer__inner--wrapper-<?php echo esc_html( get_theme_mod( 'footer_content_wrapper', 'medium' ) ); ?>">
			<?php if ( $columns > 0 ) : ?>
			<div class="footer__columns-container footer__columns-container--<?php echo esc_attr( $columns ); ?>">
				<?php for ( $i = 1; $i <= $columns; $i++ ) : ?>
					<?php $column_width = get_theme_mod( 'footer_column_' . $i . '_width', 'one-third' ); ?>
					<div class="footer__column footer__column--<?php echo esc_attr( $column_width ); ?>">
						<?php if ( is_active_sidebar( 'footer-' . $i ) ) : ?>
							<div class="footer-sidebar">
								<?php dynamic_sidebar( 'footer-' . $i ); ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endfor; ?>
			</div>
			<?php endif; ?>
			<?php
			$wordpress_link = __( 'https://wordpress.org', 'runway' );

			/* translators: %s: CMS name, i.e. WordPress. */
			$wordpress_link_text = sprintf( __( 'Proudly powered by %s.', 'runway' ), 'WordPress' );

			/* translators: 1: Theme name, 2: Theme author. */
			$theme_author_text = sprintf( __( '%1$s theme by %2$s.', 'runway' ), 'Runway', 'Desiblox' );
			?>
			<div class='footer__info'>
				<span class='footer__info-wordpress'><a href='<?php echo esc_url( $wordpress_link ); ?>'><?php echo esc_html( $wordpress_link_text ); ?></a></span>
				<span class='footer__info-theme-author'><?php echo esc_html( $theme_author_text ); ?></span>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>
