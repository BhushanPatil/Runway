<?php
/**
 * Template part for displaying pagination.
 *
 * @package Runway
 */

get_template_part( 'template-parts/pagination/pagination', get_theme_mod( 'pagination', 'numeric' ) );
