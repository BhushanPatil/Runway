<?php
/**
 * Additional features to allow styling of the templates
 *
 * @package Runway
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function runway_body_classes( $classes ) {

	// Add class for layout width.
	$classes[] = 'layout-' . get_theme_mod( 'layout_width', 'medium' );

	// Add tagline class.
	$show_tagline = get_theme_mod( 'header_show_tagline', true );
	if ( $show_tagline ) {
		$classes[] = 'with-header-tagline';
	}

	// Add header layout classes class.
	if ( get_theme_mod( 'header_full_width', true ) ) {
		$classes[] = 'header-width-full';
	} else {
		$classes[] = 'header-width-narrow';
	}

	return $classes;
}
add_filter( 'body_class', 'runway_body_classes' );

/**
 * Display the classes for the post div.
 *
 * @param string       $block   A BEM block class name.
 * @param string|array $class   One or more classes to add to the class list.
 * @param int|WP_Post  $post_id Optional. Post ID or post object. Defaults to the global `$post`.
 */
function runway_post_class( $block = '', $class = '', $post_id = null ) {

	$classes = get_post_class( $class, $post_id );

	if ( $block ) {

		if ( ! is_array( $class ) ) {
			$class = preg_split( '#\s+#', $class );
		} else {
			$class = array();
		}

		$new_classes = array( $block );
		foreach ( $classes as $c ) {
			if ( $block === $c || 'hentry' === $c || preg_match( '/' . $block . '--\S+/', $c ) ) {
				$new_classes[] = $c;
			} elseif ( 'post' === $c ) {
				continue;
			} elseif ( preg_match( '/post-\d+/', $c ) ) {
				$new_classes[] = str_replace( 'post', $block . '-', $c );
			} else {
				$new_classes[] = $block . '--' . $c;
			}
		}
		$classes = $new_classes;

		if ( has_post_thumbnail( $post_id ) ) {
			$classes[] = $block . '--has-thumbnail';
		} else {
			$classes[] = $block . '--no-thumbnail';
		}
	}

	// Separates classes with a single space, collates classes for post DIV.
	echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"';
}

/**
 * Filter the excerpt length to 30 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function runway_excerpt_length( $length ) {
	return get_theme_mod( 'post_excerpt_length', 20 );
}
add_filter( 'excerpt_length', 'runway_excerpt_length' );

/**
 * Replaces the excerpt "Read More" text by a link.
 *
 * @param string $more Link to single post/page.
 * @return string Custom excerpt read more link.
 */
function runway_more_link( $more ) {
	/* translators: 1: Post title */
	$title = sprintf( __( 'Continue reading: "%s"', 'runway' ), get_the_title() );
	$link  = '<a class="more-link__a" href="' . esc_url( get_permalink() ) . '" aria-label="' . esc_attr( $title ) . '">' . esc_html__( 'read more', 'runway' ) . '<i class="material-icons more-link__icon">&#xE5CC;</i></a>';
	return '<span class="more-link"><span class="more-link__ellipses">&hellip;</span>' . $link . '</span>';
}
add_filter( 'excerpt_more', 'runway_more_link' );
add_filter( 'the_content_more_link', 'runway_more_link' );

/**
 * Filters the anchor tag attributes for the previous posts page link.
 *
 * @param string $atts Attributes for the anchor tag.
 * @return string Modified attributes for the anchor tag.
 */
function runway_previous_posts_link_attributes( $atts ) {
	return $atts . 'class="button button--text-primary"';
}
add_filter( 'previous_posts_link_attributes', 'runway_previous_posts_link_attributes' );

/**
 * Filters the anchor tag attributes for the next posts page link.
 *
 * @param string $atts Attributes for the anchor tag.
 * @return string Modified attributes for the anchor tag.
 */
function runway_next_posts_link_attributes( $atts ) {
	return $atts . 'class="button button--text-primary"';
}
add_filter( 'next_posts_link_attributes', 'runway_next_posts_link_attributes' );

/**
 * Add wrapper class to WYSIWYG editor content.
 *
 * @param string $content Content of the widget.
 * @return string Modified content of the widget.
 */
function runway_wysiwyg_content( $content ) {
	return '<div class="wysiwyg">' . $content . '</div>';
}
add_filter( 'the_content', 'runway_wysiwyg_content' );
add_filter( 'comment_text', 'runway_wysiwyg_content' );
add_filter( 'widget_text', 'runway_wysiwyg_content' );
