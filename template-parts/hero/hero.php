<?php
/**
 * Template part for displaying hero section.
 *
 * @package Runway
 */

$bg_color       = get_theme_mod( 'header_bg_color', '#212121' );
$bg_img         = get_theme_mod( 'header_bg_img', '' );
$bg_img_opacity = get_theme_mod( 'header_bg_img_opacity', 0.75 );
$content        = get_theme_mod( 'hero_content', '' );
$slider_style   = get_theme_mod( 'hero_slider_style', 'multiple-items' );

$classes = array( 'hero' );
if ( $content && is_front_page() && is_home() ) {
	$classes[] = 'hero--' . $content;
	if ( 'slider' === $content ) {
		$classes[] = 'hero--slider-' . $slider_style;
	}
}

$bg_style = array();
$bg_style[] = 'background-color:' . $bg_color . ';';
?>
<div class="<?php echo esc_attr( join( ' ', $classes ) ); ?>">
	<div class="hero__inner">
		<div class="hero__bg" style="<?php echo esc_attr( join( ';', $bg_style ) ); ?>">
			<?php if ( $bg_img ) : ?>
				<img class="hero__bg-img" src="<?php echo esc_url( $bg_img ); ?>" alt="<?php echo esc_attr__( 'Site hero banner', 'runway' ); ?>" style="background-image:url(<?php echo esc_url( $bg_img ); ?> );opacity:<?php echo esc_attr( $bg_img_opacity ); ?>" />
			<?php endif; ?>
		</div>
		<?php
		if ( is_front_page() && is_home() ) :
			get_template_part( 'template-parts/hero/hero', $content );
		endif;
		?>
	</div>
</div>
