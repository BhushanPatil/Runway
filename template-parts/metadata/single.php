<?php
/**
 * Template part for displaying a blog post full metadata
 *
 * @package Runway
 */

$category          = get_the_category();
$author_id         = get_the_author_meta( 'ID' );
$author_name       = get_the_author();
$author_post_count = count_user_posts( $author_id );
$author_posts_url  = get_author_posts_url( $author_id );
$date              = get_the_date( get_option( 'date_format' ) );
$date_w3c          = get_the_date( DATE_W3C );
?>
<div class="metadata">
	<?php if ( ! empty( $category ) ) : ?>
	<span class="metadata__category">
		<a class="metadata__category-link" href="<?php echo esc_attr( get_category_link( $category[0]->cat_ID ) ); ?>"><?php echo esc_html( $category[0]->cat_name ); ?></a>
	</span>
	<?php endif; ?>
	<span class="metadata__author">
		<span class="metadata__author-image">
			<a class="metadata__author-image-link" href="<?php echo esc_url( $author_posts_url ); ?>">
				<?php
				echo get_avatar(
					$author_id,
					36,
					null,
					sprintf(
						/* translators: %s: Post author display name or username, i.e. John Smith, admin. */
						__( 'Profile picture of post author "%s"', 'runway' ),
						$author_name
					),
					array(
						'class' => 'metadata__author-image-tag',
					)
				);
				?>
			</a>
		</span>
		<span class="metadata__author-info">
			<span class="metadata__author-name">
				<a class="metadata__author-name-link" href="<?php echo esc_url( $author_posts_url ); ?>"><?php echo esc_html( $author_name ); ?></a>
			</span>
			<span class="metadata__author-posts-count">
				<a class="metadata__author-posts-count-link" href="<?php echo esc_url( $author_posts_url ); ?>">
					<?php
					echo esc_html(
						sprintf(
							/* translators: %s: Number of posts author has written , i.e. 1, 23. */
							_n(
								'%s Post',
								'%s Posts',
								$author_post_count,
								'runway'
							),
							number_format_i18n( $author_post_count )
						)
					);
					?>
				</a>
			</span>
		</span>
	</span>
	<span class="metadata__datetime">
		<i class="material-icons metadata__datetime-icon">&#xE192;</i>
		<time class="metadata__datetime-tag" datetime="<?php echo esc_attr( $date_w3c ); ?>"><?php echo esc_html( $date ); ?></time>
	</span>
</div>
