<?php
/**
 * The header for our theme
 *
 * @package Runway
 */

$bg_color        = get_theme_mod( 'header_bg_color', '#212121' );
$text_colors     = get_theme_mod( 'header_text_colors', 'auto' );
$content_wrapper = get_theme_mod( 'header_content_wrapper', 'large' );
$show_tagline    = get_theme_mod( 'header_show_tagline', true );
$social_links    = get_theme_mod( 'header_social_links', array( 'facebook', 'googleplus', 'instagram', 'twitter' ) );

$header_classes  = 'header';
$header_classes .= ' header--wrapper-' . $content_wrapper;

if ( 'auto' === $text_colors ) {
	$lightness = runway_rgb_to_hsl( runway_html_to_rgb( $bg_color ) )->lightness;
	if ( ( 255 / 2 ) > $lightness ) {
		$header_classes .= ' text-colors-light';
	}
} else {
	$header_classes .= ' text-colors-' . $text_colors;
}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="runway-js-hook-media-query" style="display:none"></div>
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'runway' ); ?></a>
	<header class="<?php echo esc_attr( $header_classes ); ?>" role="banner">
		<div class="header__inner">
			<div class="header__top">
				<div class="header__top-group-1">
					<button class="header__toggle header__toggle--menu"><i class="material-icons" aria-hidden="true">&#xE5D2;</i><span class="screen-reader-text"><?php esc_html_e( 'Open navigation menu', 'runway' ); ?></span></button>
					<div class="header__branding">
						<?php if ( is_front_page() && is_home() ) : ?>
						<h1 class="header__branding-title">
							<a class="header__branding-title-link" href="<?php echo esc_url( home_url() ); ?>" rel="home"><?php echo esc_html( get_bloginfo( 'name' ) ); ?></a>
						</h1>
						<?php else : ?>
						<p class="header__branding-title">
							<a class="header__branding-title-link" href="<?php echo esc_url( home_url() ); ?>" rel="home"><?php echo esc_html( get_bloginfo( 'name' ) ); ?></a>
						</p>
						<?php endif; ?>
						<?php if ( $show_tagline ) : ?>
						<p class="header__branding-tagline"><?php echo esc_html( get_bloginfo( 'description', 'display' ) ); ?></p>
						<?php endif; ?>
					</div>
					<button class="header__toggle header__toggle--search"><i class="material-icons" aria-hidden="true">&#xE8B6;</i><span class="screen-reader-text"><?php esc_html_e( 'Open search', 'runway' ); ?></span></button>
				</div>
				<div class="header__top-group-2"></div>
			</div>
			<div class="header__nav">
				<div class="header__nav-group-1">
					<nav class="header__primary-menu" role="navigation" aria-label="<?php esc_attr_e( 'Primary menu', 'runway' ); ?>">
						<?php
						wp_nav_menu( array(
							'container'      => 'ul',
							'menu_class'     => 'header__menu header__menu--primary',
							'theme_location' => 'header-primary',
							'walker'         => new Runway_Walker_Nav_Menu(),
						) );
						?>
					</nav>
					<nav class="header__secondary-menu" role="navigation" aria-label="<?php esc_attr_e( 'Secondary menu', 'runway' ); ?>">
						<?php
						wp_nav_menu( array(
							'container'      => 'ul',
							'menu_class'     => 'header__menu header__menu--secondary header__menu--right',
							'theme_location' => 'header-secondary',
							'walker'         => new Runway_Walker_Nav_Menu(),
						) );
						?>
					</nav>
					<?php if ( ! empty( $social_links ) ) : ?>
					<nav class="header__social" role="navigation" aria-label="<?php esc_attr_e( 'Social links menu', 'runway' ); ?>">
						<p class="header__social-heading"><?php esc_html_e( 'Follow', 'runway' ); ?></p>
						<?php runway_social_links( $social_links, 'header__social-links' ); ?>
					</nav>
					<?php endif; ?>
				</div>
				<div class="header__nav-group-2">
					<div class="header__search">
						<?php runway_search_form( 'header__search-form' ); ?>
					</div>
				</div>
			</div>
		</div>
	</header>
	<?php get_template_part( 'template-parts/hero/hero' ); ?>
