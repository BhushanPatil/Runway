<?php
/**
 * The template for displaying all single posts
 *
 * @package Runway
 */

$show_sidebar = get_theme_mod( 'post_show_sidebar', false );

$section_clases = 'section';
if ( $show_sidebar ) {
	$section_clases .= ' section--sidebar-' . get_theme_mod( 'post_sidebar_width', 'one-fourth' );
}

get_header();
?>
<div class="main">
	<div class="main__inner">
		<div class="<?php echo esc_attr( $section_clases ); ?>">
			<!-- <div class="section__header">
				<h1 class="section__header-entry-title">
					<span class="section__header-entry-title-text"><?php the_title(); ?></span>
				</h1>
			</div> -->
			<div class="section__content">
				<main id="main" class="section__primary-area" role="main">
					<?php
					if ( have_posts() ) :
						while ( have_posts() ) :
							the_post();
							get_template_part( 'template-parts/post/content', get_post_format() );
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						endwhile;
					endif;
					?>
				</main>
				<?php if ( $show_sidebar ) : ?>
				<aside class="section__secondary-area" role="complementary">
					<?php dynamic_sidebar( 'sidebar-post' ); ?>
				</aside>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
