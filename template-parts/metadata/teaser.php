<?php
/**
 * Template part for displaying a blog post metadata in short
 *
 * @package Runway
 */

$category         = get_the_category();
$permalink        = get_permalink();
$author_id        = get_the_author_meta( 'ID' );
$author_name      = get_the_author();
$author_posts_url = get_author_posts_url( $author_id );
$date             = get_the_date( get_option( 'date_format' ) );
$date_w3c         = get_the_date( DATE_W3C );
?>
<div class="metadata">
	<!-- <span class="metadata__cta">
		<a class="metadata__cta-link button button--text-primary button--ghost" href="<?php echo esc_url( $permalink ); ?>"><?php echo esc_html__( 'View Post', 'runway' ); ?></a>
	</span> -->
	<?php if ( ! empty( $category ) ) : ?>
	<span class="metadata__category">
		<a class="metadata__category-link" href="<?php echo esc_url( get_category_link( $category[0]->cat_ID ) ); ?>"><?php echo esc_html( $category[0]->cat_name ); ?></a>
	</span>
	<?php endif; ?>
	<span class="metadata__author">
		<span class="metadata__author-image">
			<a class="metadata__author-image-link" href="<?php echo esc_url( $author_posts_url ); ?>">
				<?php
				echo get_avatar(
					$author_id,
					36,
					null,
					sprintf(
						/* translators: %s: Post author display name or username, i.e. John Smith, admin. */
						__( 'Post author %s"s profile picture', 'runway' ),
						$author_name
					),
					array(
						'class' => 'metadata__author-image-tag',
					)
				);
				?>
			</a>
		</span>
		<span class="metadata__author-info">
			<span class="metadata__author-name">
				<a class="metadata__author-name-link" href="<?php echo esc_url( $author_posts_url ); ?>"><?php echo esc_html( $author_name ); ?></a>
			</span>
			<span class="metadata__author-datetime">
				<a class="metadata__author-datetime-link" href="<?php echo esc_url( $permalink ); ?>">
					<time class="metadata__author-datetime-tag" datetime="<?php echo esc_attr( $date_w3c ); ?>"><?php echo esc_html( $date ); ?></time>
				</a>
			</span>
		</span>
	</span>
</div>
