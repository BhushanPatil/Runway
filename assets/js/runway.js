( function( $ ) {
	'use strict';
	var runway = {
		mediaQuery: null,
		mediaQueryPrevious: null,
		fontSize: window.getComputedStyle( document.documentElement ).getPropertyValue( 'font-size' ),
		lineHeight: window.getComputedStyle( document.documentElement ).getPropertyValue( 'line-height' ),
		transitionSpeed: 300,
		transitionEasing: $.bez([ 0.4, 0, 0.2, 1 ]),
		mediaQueryRefresh: function() {

			var htmlStyle     = window.getComputedStyle( document.documentElement ),
				mediaQueryNew = window.getComputedStyle( document.getElementById( 'runway-js-hook-media-query' ), '::before' ).getPropertyValue( 'content' ).slice( 1, -1 ),
				primaryMenu   = document.querySelector( '.header__primary-menu' ),
				secondaryMenu = document.querySelector( '.header__secondary-menu' ),
				search        = document.querySelector( '.header__search' ),
				social        = document.querySelector( '.header__social' ),
				topGroup2     = document.querySelector( '.header__top-group-2' ),
				navGroup1     = document.querySelector( '.header__nav-group-1' ),
				navGroup2     = document.querySelector( '.header__nav-group-2' ),
				bodyClasses   = document.body.classList,
				nav           = document.querySelector( '.header__nav' ),
				navClasses    = nav.classList;

			runway.fontSize = parseInt( htmlStyle.getPropertyValue( 'font-size' ) ),
			runway.lineHeight = parseInt( htmlStyle.getPropertyValue( 'line-height' ) );

			if ( mediaQueryNew !== runway.mediaQuery && mediaQueryNew !== runway.mediaQueryPrevious ) {
				runway.mediaQuery         = mediaQueryNew;
				runway.mediaQueryPrevious = runway.mediaQuery;
			} else {
				runway.mediaQuery = null;
			}

			if ( 'mobile' === runway.mediaQuery  ) {

				if ( primaryMenu ) {
					navGroup1.appendChild( primaryMenu );
				}
				if ( secondaryMenu ) {
					navGroup1.appendChild( secondaryMenu );
				}
				if ( social ) {
					navGroup1.appendChild( social );
				}
				if ( search ) {
					navGroup2.appendChild( search );
				}

				if ( navClasses.contains( '.header__nav--show-group-1' ) || navClasses.contains( '.header__nav--show-group-2' ) ) {
					runway.scrollToTop( undefined, 0 );
					bodyClasses.add( 'overflow-hidden' );
				}

			} else if ( 'desktop' === runway.mediaQuery ) {

				if ( primaryMenu ) {
					navGroup1.appendChild( primaryMenu );
				}
				if ( secondaryMenu ) {
					navGroup2.appendChild( secondaryMenu );
				}
				if ( search ) {
					topGroup2.appendChild( search );
				}
				if ( social ) {
					topGroup2.appendChild( social );
				}

				bodyClasses.remove( 'overflow-hidden' );
			}

		},
		headerNavToggles: function() {

			var bodyClasses =  document.body.classList,
				nav = document.querySelector( '.header__nav' ),
				navClasses = nav.classList,
				menuToggler = document.querySelector( '.header__toggle--menu' ),
				searchToggler = document.querySelector( '.header__toggle--search' );

			function toggleMenu() {
				runway.scrollToTop( function() {
					if ( navClasses.contains( 'header__nav--show-group-2' ) ) {
						navClasses.remove( 'header__nav--show-group-2' );
						navClasses.add( 'header__nav--show-group-1' );
						bodyClasses.add( 'overflow-hidden' );
					} else {
						navClasses.toggle( 'header__nav--show-group-1' );
						bodyClasses.toggle( 'overflow-hidden' );
					}
				});
			}

			function toggleSearch() {
				runway.scrollToTop( function() {
					if ( navClasses.contains( 'header__nav--show-group-1' ) ) {
						navClasses.remove( 'header__nav--show-group-1' );
						navClasses.add( 'header__nav--show-group-2' );
						bodyClasses.add( 'overflow-hidden' );
					} else {
						navClasses.toggle( 'header__nav--show-group-2' );
						bodyClasses.toggle( 'overflow-hidden' );
					}
				});
			}

			menuToggler.addEventListener( 'click', toggleMenu );

			searchToggler.addEventListener( 'click', toggleSearch );
		},
		headerMenuToggles: function() {

			var toggles = $( '.header__menu-toggle' ),
				slideOptions = {
					duration: runway.transitionSpeed,
					easing: runway.transitionEasing,
					complete: function() {
						if ( this.classList.contains( 'runway-js-slide-on' ) ) {
							this.classList.remove( 'runway-js-slide-on' );
							this.style.removeProperty( 'display' );
						} else {
							this.classList.add( 'runway-js-slide-on' );
						}
					}
				};

			toggles.on( 'click', function() {
				var toggle = $( this );
				toggle.siblings( '.header__menu-sub' ).slideToggle( slideOptions )
					.parent().toggleClass( 'header__menu-item--active' )
					.siblings( '.header__menu-item--active' ).removeClass( 'header__menu-item--active' )
					.children( '.header__menu-sub' ).slideUp( slideOptions );
				toggle.closest( '.header__menu' ).parent().siblings()
					.find( '.header__menu-item--active' ).removeClass( 'header__menu-item--active' )
					.children( '.header__menu-sub' ).slideUp( slideOptions );
			});

		},
		searchForm: function() {

			$( '.search-form, .header__search' ).each( function() {

				var searchForm = $( this ),
					searchFormClass = searchForm.attr( 'class' ).split( ' ' )[0],
					searchFormInput = searchForm.find( 'input[type="search"]' );

				if ( searchFormInput.val() ) {
					searchForm.addClass( searchFormClass + '--has-value' );
				}

				searchForm.on({
					blur: function() {
						if ( searchFormInput.val() ) {
							searchForm.addClass( searchFormClass + '--has-value' );
						} else {
							searchForm.removeClass( searchFormClass + '--focus' );
						}
					},
					focusin: function() {
						searchForm.addClass( searchFormClass + '--focus' ).removeClass( searchFormClass + '--hover' );
					},
					focusout: function() {
						searchForm.removeClass( searchFormClass + '--focus' );
						searchForm.removeClass( searchFormClass + '--has-value' );
						if ( searchFormInput.val() ) {
							searchForm.addClass( searchFormClass + '--has-value' );
						}
					},
					mouseenter: function() {
						if ( ! searchForm.hasClass( searchFormClass + '--has-value' ) ) {
							searchForm.addClass( searchFormClass + '--hover' );
						}
					},
					mouseleave: function() {
						searchForm.removeClass( searchFormClass + '--hover' );
					}
				});
			});

		},
		slick: function() {

			$( '.slick-slider' ).slick();
			$( '.slick-slider-with-arrows-on-slide' ).on( 'click', '.slick-slide-content', function( e ) {

				var clickedSlide = $( this ).parent(),
					slider = clickedSlide.closest( '.slick-slider' ),
					clickedSlideIndex = clickedSlide.data( 'slick-index' ),
					currentSlideIndex = slider.slick( 'slickCurrentSlide' );

				e.stopPropagation();

				if ( currentSlideIndex !== clickedSlideIndex ) {
					slider.slick( 'slickGoTo', clickedSlideIndex );
				}
			});

		},
		masonry: function() {

			$( '.masonry' ).masonry({
				itemSelector: '.masonry__item',
				columnWidth: '.masonry__item:last-child',
				percentPosition: true
			});

		},
		scrollToTop: function( callback, speed, easing ) {

			var scrollElement = null;

			callback = ( 'undefined' !== typeof callback ) ? callback : null;
			speed    = ( 'undefined' !== typeof speed ) ? speed : runway.transitionSpeed;
			easing   = ( 'undefined' !== typeof easing ) ? easing : runway.transitionEasing;

			if ( 0 < document.body.scrollTop ) {
				scrollElement = document.body; // Safari
			} else if ( 0 < document.documentElement.scrollTop ) {
				scrollElement = document.documentElement; // Chrome, Firefox, IE and Opera
			}

			if ( scrollElement ) {
				$( scrollElement ).animate({ scrollTop: 0 }, speed, easing, callback );
			} else {
				callback();
			}

		},
		dev: function() {

			var bodyClasses = document.body.classList,
				html        = document.documentElement,
				Height      = null,
				devBar      = document.createElement( 'div' ),
				masonry     = document.createElement( 'span' );

			devBar.setAttribute( 'class', 'dev-bar' );
			devBar.innerHTML += '<span class="dev-option toggle-baseline">Baseline</span>';
			document.body.appendChild( devBar );

			document.querySelector( '.toggle-baseline' ).addEventListener( 'click', function() {
				Height = Math.max(
					document.body.scrollHeight,
					document.body.offsetHeight,
					html.clientHeight,
					html.scrollHeight,
					html.offsetHeight
				);
				document.body.setAttribute( 'style', 'height:' + Height + 'px' );
				if ( bodyClasses.contains( 'baseline--full' ) ) {
					bodyClasses.remove( 'baseline--full' );
					bodyClasses.add( 'baseline--half' );
				} else if ( bodyClasses.contains( 'baseline--half' ) ) {
					bodyClasses.remove( 'baseline' );
					bodyClasses.remove( 'baseline--half' );
				} else {
					bodyClasses.add( 'baseline' );
					bodyClasses.add( 'baseline--full' );
				}
			});

			masonry.setAttribute( 'class', 'dev-option masonry-reload' );
			masonry.appendChild( document.createTextNode( 'masonry' ) );
			devBar.insertBefore( masonry, devBar.childNodes[0]);

			document.querySelector( '.masonry-reload' ).addEventListener( 'click', function() {
				$( '.masonry' ).masonry({
					itemSelector: '.masonry__item',
					percentPosition: true
				});
			});
		}
	};
	$( document ).ready( function() {
		runway.mediaQueryRefresh();
		runway.headerNavToggles();
		runway.headerMenuToggles();
		runway.searchForm();
		runway.slick();
		runway.masonry();
		runway.dev();
	});
	$( window ).on({
		load: function() {
			runway.masonry();
		},
		resize: function() {
			var bodyClasses = document.body.classList;
			bodyClasses.add( 'no-transition-all' );
			runway.mediaQueryRefresh();

			bodyClasses.remove( 'no-transition-all' );
		},
		orientationchange: function() {
			var bodyClasses = document.body.classList;
			bodyClasses.add( 'no-transition-all' );
			runway.mediaQueryRefresh();

			bodyClasses.remove( 'no-transition-all' );
		}
	});
}( jQuery ) );
