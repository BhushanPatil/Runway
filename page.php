<?php
/**
 * The template for displaying all pages
 *
 * @package Runway
 */

$show_sidebar    = get_theme_mod( 'page_show_sidebar', true );
$entry_classes   = array();
$entry_classes[] = 'entry--title-' . get_theme_mod( 'post_title_align', 'center' );

get_header();
?>
<div class="main">
	<div class="main__inner">
		<div class="section">
			<div class="section__content">
				<main id="main" class="section__primary-area" role="main">
					<?php
					if ( have_posts() ) :
						while ( have_posts() ) :
							the_post();
							get_template_part( 'template-parts/page/content' );
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						endwhile;
					endif;
					?>
				</main>
				<?php if ( $show_sidebar ) : ?>
				<aside class="section__secondary-area" role="complementary">
					<?php dynamic_sidebar( 'sidebar-page' ); ?>
				</aside>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
