<?php
/**
 * Theme customizer functions and definitions
 *
 * @package Runway
 */

/**
 * Enqueue customize controls scripts.
 */
function runway_customize_controls_enqueue() {
	wp_enqueue_script(
		'runway-customize-controls-controls-js',
		get_theme_file_uri( '/assets/js/customize-controls.js' ),
		array(),
		null,
		true
	);
}
add_action( 'customize_controls_enqueue_scripts', 'runway_customize_controls_enqueue' );

/**
 * Enqueue customize preview scripts.
 */
function runway_customize_preview_enqueue() {
	wp_enqueue_script(
		'runway-customize-preview-js',
		get_template_directory_uri() . '/assets/js/customize-preview.js',
		array( 'jquery', 'customize-preview' ),
		null,
		true
	);
}
add_action( 'customize_preview_init', 'runway_customize_preview_enqueue' );

/**
 * To add, remove, or modify any Customizer object, and to access the Customizer Manager.
 *
 * @param object $wp_customize Customize Manager object.
 */
function runway_customize_register( $wp_customize ) {

	/**
	 * Load custom controls.
	 */
	require get_parent_theme_file_path( 'inc/class-runway-checkbox-multiple-control.php' );
	require get_parent_theme_file_path( 'inc/class-runway-image-select-control.php' );

	/**
	 * Panels and Sections
	 */
	$priority = 21;
	$wp_customize->add_section( 'header', array(
		'priority' => $priority++,
		'title'    => esc_html__( 'Header', 'runway' ),
	) );
	$wp_customize->add_section( 'hero', array(
		'priority' => $priority++,
		'title'    => esc_html__( 'Hero', 'runway' ),
	) );
	$wp_customize->get_section( 'static_front_page' )->priority = $priority++;
	$wp_customize->get_section( 'static_front_page' )->title    = esc_html__( 'Homepage', 'runway' );
	$wp_customize->add_section( 'post', array(
		'priority' => $priority++,
		'title'    => esc_html__( 'Post', 'runway' ),
	) );
	$wp_customize->add_section( 'page', array(
		'priority' => $priority++,
		'title'    => esc_html__( 'Page', 'runway' ),
	) );
	$wp_customize->add_section( 'comments', array(
		'priority' => $priority++,
		'title'    => esc_html__( 'Comments', 'runway' ),
	) );
	$wp_customize->add_section( 'layout', array(
		'priority' => $priority++,
		'title'    => esc_html__( 'Layout', 'runway' ),
	) );
	$wp_customize->add_section( 'look_and_feel', array(
		'priority' => $priority++,
		'title'    => esc_html__( 'Look &amp; Feel', 'runway' ),
	) );
	$wp_customize->add_section( 'typography', array(
		'priority' => $priority++,
		'title'    => esc_html__( 'Typography', 'runway' ),
	) );
	$wp_customize->add_panel( 'social_media', array(
		'priority' => $priority++,
		'title'    => __( 'Social Media', 'runway' ),
	) );
	$wp_customize->add_section( 'social_links', array(
		'panel' => 'social_media',
		'title' => esc_html__( 'Profile / Page URLs', 'runway' ),
	) );
	$wp_customize->add_section( 'social_sharing', array(
		'panel' => 'social_media',
		'title' => esc_html__( 'Sharing Buttons', 'runway' ),
	) );
	$wp_customize->add_section( 'footer', array(
		'priority' => $priority++,
		'title'    => esc_html__( 'Footer', 'runway' ),
	) );

	/**
	 * Header section settings and controls.
	 */
	$wp_customize->add_setting( 'header_full_width', array(
		'default'           => true,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'header_full_width', array(
		'label'   => esc_html__( 'Full Width', 'runway' ),
		'section' => 'header',
		'type'    => 'checkbox',
	) );

	$wp_customize->add_setting( 'header_bg_color', array(
		'default'           => '#212121',
		'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_bg_color', array(
		'label'   => esc_html__( 'Background Color', 'runway' ),
		'section' => 'header',
	) ) );

	$wp_customize->add_setting( 'header_bg_img', array(
		'default'           => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_bg_img', array(
		'label'   => esc_html__( 'Background Image', 'runway' ),
		'section' => 'header',
	) ) );

	$wp_customize->add_setting( 'header_bg_img_opacity', array(
		'default'           => 0.75,
		'sanitize_callback' => 'runway_sanitize_float',
	) );
	$wp_customize->add_control( 'header_bg_img_opacity', array(
		'label'           => esc_html__( 'Background Image Opacity', 'runway' ),
		'section'         => 'header',
		'type'            => 'range',
		'active_callback' => function () {
			if ( '' === get_theme_mod( 'header_bg_img', '' ) ) {
				return false;
			}
			return true;
		},
		'input_attrs'     => array(
			'min'  => 0,
			'max'  => 1,
			'step' => 0.05,
		),
	) );

	$wp_customize->add_setting( 'header_text_colors', array(
		'default'           => 'dark',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'header_text_colors', array(
		'label'   => esc_html__( 'Text Colors', 'runway' ),
		'section' => 'header',
		'type'    => 'radio',
		'choices' => array(
			'auto'  => esc_html__( 'Auto', 'runway' ),
			'light' => esc_html__( 'Light', 'runway' ),
			'dark'  => esc_html__( 'Dark', 'runway' ),
		),
	) );

	$wp_customize->add_setting( 'header_content_wrapper', array(
		'default'           => 'large',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( new Runway_Image_Select_Control( $wp_customize, 'header_content_wrapper', array(
		'choices' => array(
			'full'   => array(
				'label' => esc_html__( 'Wrapper Width Full', 'runway' ),
				'url'   => get_template_directory_uri() . '/assets/img/header-content-wrapper-1.jpg',
				'width' => 90,
			),
			'large'  => array(
				'label' => esc_html__( 'Wrapper Width Large', 'runway' ),
				'url'   => get_template_directory_uri() . '/assets/img/header-content-wrapper-2.jpg',
				'width' => 90,
			),
			'medium' => array(
				'label' => esc_html__( 'Wrapper Width Medium', 'runway' ),
				'url'   => get_template_directory_uri() . '/assets/img/header-content-wrapper-3.jpg',
				'width' => 90,
			),
			'small'  => array(
				'label' => esc_html__( 'Wrapper Width Small', 'runway' ),
				'url'   => get_template_directory_uri() . '/assets/img/header-content-wrapper-4.jpg',
				'width' => 90,
			),
		),
		'label'   => esc_html__( 'Content Wrapper Width', 'runway' ),
		'section' => 'header',
	) ) );

	$wp_customize->add_setting( 'header_show_tagline', array(
		'default'           => true,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'header_show_tagline', array(
		'label'   => esc_html__( 'Show Tagline', 'runway' ),
		'section' => 'header',
		'type'    => 'checkbox',
	) );

	$wp_customize->add_setting( 'header_social_links', array(
		'default'           => array( 'facebook', 'googleplus', 'instagram', 'twitter' ),
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Runway_Checkbox_Multiple_Control( $wp_customize, 'header_social_links', array(
		'description' => esc_html__( 'Select which social media links to display in site header section. Theme will not display selected social media link, if link URL is not set. You can set social media link URL in "Social Media > Profile / Page URLs" section.', 'runway' ),
		'label'       => esc_html__( 'Social Media Links', 'runway' ),
		'section'     => 'header',
		'choices'     => array(
			'behance'    => esc_html__( 'Behance', 'runway' ),
			'bloglovin'  => esc_html__( 'Bloglovin', 'runway' ),
			'dribbble'   => esc_html__( 'Dribbble', 'runway' ),
			'facebook'   => esc_html__( 'Facebook', 'runway' ),
			'flickr'     => esc_html__( 'Flickr', 'runway' ),
			'foursquare' => esc_html__( 'Foursquare', 'runway' ),
			'github'     => esc_html__( 'GitHub', 'runway' ),
			'gitlab'     => esc_html__( 'GitLab', 'runway' ),
			'googleplus' => esc_html__( 'Google+', 'runway' ),
			'instagram'  => esc_html__( 'Instagram', 'runway' ),
			'linkedin'   => esc_html__( 'LinkedIn', 'runway' ),
			'medium'     => esc_html__( 'Medium', 'runway' ),
			'rss'        => esc_html__( 'RSS Feeds', 'runway' ),
			'pinterest'  => esc_html__( 'Pinterest', 'runway' ),
			'snapchat'   => esc_html__( 'Snapchat', 'runway' ),
			'soundcloud' => esc_html__( 'SoundCloud', 'runway' ),
			'tumblr'     => esc_html__( 'Tumblr', 'runway' ),
			'twitch'     => esc_html__( 'Twitch', 'runway' ),
			'twitter'    => esc_html__( 'Twitter', 'runway' ),
			'vimeo'      => esc_html__( 'Vimeo', 'runway' ),
			'youtube'    => esc_html__( 'YouTube', 'runway' ),
		),
	) ) );

	/**
	 * Hero section settings and controls.
	 */
	$wp_customize->add_setting( 'hero_content', array(
		'default'           => '',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'hero_content', array(
		'label'   => esc_html__( 'Content', 'runway' ),
		'section' => 'hero',
		'type'    => 'radio',
		'choices' => array(
			''       => esc_html__( 'None', 'runway' ),
			'intro'  => esc_html__( 'Site Intro', 'runway' ),
			'slider' => esc_html__( 'Featured Posts Slider', 'runway' ),
		),
	) );

	$wp_customize->add_setting( 'hero_intro_title', array(
		'default'           => get_bloginfo( 'name' ),
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'hero_intro_title', array(
		'label'           => esc_html__( 'Intro Title', 'runway' ),
		'section'         => 'hero',
		'type'            => 'text',
		'active_callback' => function () {
			if ( 'intro' === get_theme_mod( 'hero_content', 'none' ) ) {
				return true;
			}
			return false;
		},
	) );

	$wp_customize->add_setting( 'hero_intro_copy', array(
		'default'           => get_bloginfo( 'description' ),
		'sanitize_callback' => 'wp_kses_post',
	) );
	$wp_customize->add_control( 'hero_intro_copy', array(
		'label'           => esc_html__( 'Intro Copy', 'runway' ),
		'section'         => 'hero',
		'type'            => 'textarea',
		'active_callback' => function () {
			if ( 'intro' === get_theme_mod( 'hero_content', 'none' ) ) {
				return true;
			}
			return false;
		},
	) );

	$wp_customize->add_setting( 'hero_slider_style', array(
		'default'           => 'multiple-items',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( new Runway_Image_Select_Control( $wp_customize, 'hero_slider_style', array(
		'active_callback' => function () {
			if ( 'slider' === get_theme_mod( 'hero_content', 'none' ) ) {
				return true;
			}
			return false;
		},
		'choices'         => array(
			'single-item'    => array(
				'label' => esc_html__( 'Single Slide', 'runway' ),
				'url'   => get_template_directory_uri() . '/assets/img/hero-slider-1.jpg',
			),
			'multiple-items' => array(
				'label' => esc_html__( 'Multiple Slides', 'runway' ),
				'url'   => get_template_directory_uri() . '/assets/img/hero-slider-2.jpg',
			),
			'full'           => array(
				'label' => esc_html__( 'Full Slide', 'runway' ),
				'url'   => get_template_directory_uri() . '/assets/img/hero-slider-3.jpg',
			),
			'almost-full'    => array(
				'label' => esc_html__( 'Almost Full Slide', 'runway' ),
				'url'   => get_template_directory_uri() . '/assets/img/hero-slider-4.jpg',
			),
		),
		'label'           => esc_html__( 'Slider Style', 'runway' ),
		'section'         => 'hero',
	) ) );

	$wp_customize->add_setting( 'hero_slider_number_of_posts', array(
		'default'           => 5,
		'sanitize_callback' => 'absint',
	) );
	$wp_customize->add_control( 'hero_slider_number_of_posts', array(
		'label'           => esc_html__( 'Slider Number Of Posts', 'runway' ),
		'section'         => 'hero',
		'type'            => 'number',
		'active_callback' => function () {
			if ( 'slider' === get_theme_mod( 'hero_content', 'none' ) ) {
				return true;
			}
			return false;
		},
	) );

	$wp_customize->add_setting( 'hero_slider_autoplay', array(
		'default'           => false,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'hero_slider_autoplay', array(
		'label'           => esc_html__( 'Slider Autoplay', 'runway' ),
		'section'         => 'hero',
		'type'            => 'checkbox',
		'active_callback' => function () {
			if ( 'slider' === get_theme_mod( 'hero_content', 'none' ) ) {
				return true;
			}
			return false;
		},
	) );

	$wp_customize->add_setting( 'hero_slider_arrows', array(
		'default'           => true,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'hero_slider_arrows', array(
		'label'           => esc_html__( 'Slider Prev/Next Arrows', 'runway' ),
		'section'         => 'hero',
		'type'            => 'checkbox',
		'active_callback' => function () {
			if ( 'slider' === get_theme_mod( 'hero_content', 'none' ) ) {
				return true;
			}
			return false;
		},
	) );

	$wp_customize->add_setting( 'hero_slider_dots', array(
		'default'           => true,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'hero_slider_dots', array(
		'label'           => esc_html__( 'Slider Dots Indicators', 'runway' ),
		'section'         => 'hero',
		'type'            => 'checkbox',
		'active_callback' => function () {
			if ( 'slider' === get_theme_mod( 'hero_content', 'none' ) ) {
				return true;
			}
			return false;
		},
	) );

	$wp_customize->add_setting( 'hero_slider_fade', array(
		'default'           => true,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'hero_slider_fade', array(
		'label'           => esc_html__( 'Slider Fade Animation', 'runway' ),
		'section'         => 'hero',
		'type'            => 'checkbox',
		'active_callback' => function () {
			if ( 'slider' === get_theme_mod( 'hero_content', 'none' ) && 'multiple-items' !== get_theme_mod( 'hero_slider_style', 'multiple-items' ) ) {
				return true;
			}
			return false;
		},
	) );

	$wp_customize->add_setting( 'hero_slider_infinite', array(
		'default'           => true,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'hero_slider_infinite', array(
		'label'           => esc_html__( 'Infinite Loop Sliding', 'runway' ),
		'section'         => 'hero',
		'type'            => 'checkbox',
		'active_callback' => function () {
			if ( 'slider' === get_theme_mod( 'hero_content', 'none' ) ) {
				return true;
			}
			return false;
		},
	) );

	/**
	 * Homepage section settings and controls.
	 */

	$wp_customize->add_setting( 'homepage_show_heading', array(
		'default'           => false,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'homepage_show_heading', array(
		'label'           => esc_html__( 'Show Heading For Latest Posts', 'runway' ),
		'section'         => 'static_front_page',
		'type'            => 'checkbox',
		'active_callback' => function () {
			return 'posts' === get_option( 'show_on_front' ) ? true : false;
		},
	) );

	$wp_customize->add_setting( 'homepage_latest_posts_heading', array(
		'default'           => '',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'homepage_latest_posts_heading', array(
		'active_callback' => function () {
			if ( 'posts' === get_option( 'show_on_front' ) ) {
				if ( get_theme_mod( 'homepage_show_heading', false ) ) {
					return true;
				}
			}
			return false;
		},
		'label'           => esc_html__( 'Heading For Latest Posts', 'runway' ),
		'section'         => 'static_front_page',
		'type'            => 'text',
	) );

	$wp_customize->add_setting( 'homepage_latest_posts_subheading', array(
		'default'           => '',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'homepage_latest_posts_subheading', array(
		'active_callback' => function () {
			if ( 'posts' === get_option( 'show_on_front' ) ) {
				if ( get_theme_mod( 'homepage_show_heading', false ) ) {
					return true;
				}
			}
			return false;
		},
		'label'           => esc_html__( 'Subheading For Latest Posts', 'runway' ),
		'section'         => 'static_front_page',
		'type'            => 'text',
	) );

	$wp_customize->add_setting( 'homepage_full_width_first_post', array(
		'default'           => false,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'homepage_full_width_first_post', array(
		'label'           => esc_html__( 'Full Width For First Post', 'runway' ),
		'section'         => 'static_front_page',
		'type'            => 'checkbox',
		'active_callback' => function () {
			return 'posts' === get_option( 'show_on_front' ) ? true : false;
		},
	) );

	$wp_customize->add_setting( 'homepage_show_sidebar', array(
		'default'           => false,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'homepage_show_sidebar', array(
		'label'           => esc_html__( 'Show Sidebar', 'runway' ),
		'section'         => 'static_front_page',
		'type'            => 'checkbox',
		'active_callback' => function () {
			return 'posts' === get_option( 'show_on_front' ) ? true : false;
		},
	) );

	$wp_customize->add_setting( 'homepage_sidebar_width', array(
		'default'           => 'one-fourth',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'homepage_sidebar_width', array(
		'active_callback' => function () {
			if ( 'posts' === get_option( 'show_on_front' ) ) {
				if ( get_theme_mod( 'homepage_show_sidebar', false ) ) {
					return true;
				}
			}
			return false;
		},
		'choices'         => array(
			'one-third'  => esc_html__( 'One Third', 'runway' ),
			'one-fourth' => esc_html__( 'One Fourth', 'runway' ),
			'one-fifth'  => esc_html__( 'One Fifth', 'runway' ),
			'one-sixth'  => esc_html__( 'One Sixth', 'runway' ),
		),
		'label'           => esc_html__( 'Sidebar Width', 'runway' ),
		'section'         => 'static_front_page',
		'type'            => 'radio',
	) );

	$wp_customize->add_setting( 'post_excerpt_length', array(
		'default'           => 20,
		'sanitize_callback' => 'absint',
	) );
	$wp_customize->add_control( 'post_excerpt_length', array(
		'label'           => esc_html__( 'Post Excerpt Length', 'runway' ),
		'section'         => 'static_front_page',
		'type'            => 'number',
		'active_callback' => function () {
			return 'posts' === get_option( 'show_on_front' ) ? true : false;
		},
	) );

	$wp_customize->add_setting( 'pagination', array(
		'default'           => 'numeric',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'pagination', array(
		'active_callback' => function () {
			return 'posts' === get_option( 'show_on_front' ) ? true : false;
		},
		'label'           => esc_html__( 'Pagination', 'runway' ),
		'section'         => 'static_front_page',
		'type'            => 'radio',
		'choices'         => array(
			'numeric'   => esc_html__( 'Numeric', 'runway' ),
			'prev-next' => esc_html__( 'Previous Next', 'runway' ),
		),
	) );

	/**
	 * Post section settings and controls.
	 */
	$wp_customize->add_setting( 'post_title_align', array(
		'default'           => 'center',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'post_title_align', array(
		'label'   => esc_html__( 'Title Alignment ', 'runway' ),
		'section' => 'post',
		'type'    => 'radio',
		'choices' => array(
			'left'   => esc_html__( 'Left', 'runway' ),
			'center' => esc_html__( 'Center', 'runway' ),
			'Right'  => esc_html__( 'Right', 'runway' ),
		),
	) );

	$wp_customize->add_setting( 'post_show_sidebar', array(
		'default'           => false,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'post_show_sidebar', array(
		'label'   => esc_html__( 'Show Sidebar', 'runway' ),
		'section' => 'post',
		'type'    => 'checkbox',
	) );

	$wp_customize->add_setting( 'post_sidebar_width', array(
		'default'           => 'one-fourth',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'post_sidebar_width', array(
		'active_callback' => function () {
			return get_theme_mod( 'post_show_sidebar', false );
		},
		'choices'         => array(
			'one-third'  => esc_html__( 'One Third', 'runway' ),
			'one-fourth' => esc_html__( 'One Fourth', 'runway' ),
			'one-fifth'  => esc_html__( 'One Fifth', 'runway' ),
			'one-sixth'  => esc_html__( 'One Sixth', 'runway' ),
		),
		'label'           => esc_html__( 'Sidebar Width', 'runway' ),
		'section'         => 'post',
		'type'            => 'radio',
	) );

	/**
	 * Page section settings and controls.
	 */
	$wp_customize->add_setting( 'page_show_sidebar', array(
		'default'           => false,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'page_show_sidebar', array(
		'label'   => esc_html__( 'Show Sidebar', 'runway' ),
		'section' => 'page',
		'type'    => 'checkbox',
	) );

	/**
	 * Comments section settings and controls.
	 */
	$wp_customize->add_setting( 'comments_hide_labels', array(
		'default'           => true,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'comments_hide_labels', array(
		'description' => esc_html__( 'Hides labels in comments respond form. Hidden labels text can be readable by screen readers for better accessibility.', 'runway' ),
		'label'       => esc_html__( 'Hide Labels In Comments Form', 'runway' ),
		'section'     => 'comments',
		'type'        => 'checkbox',
	) );

	$wp_customize->add_setting( 'comments_hide_url_field', array(
		'default'           => true,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'comments_hide_url_field', array(
		'label'   => esc_html__( 'Hide Website URL Field In Comments Form', 'runway' ),
		'section' => 'comments',
		'type'    => 'checkbox',
	) );

	/**
	 * Typography section settings and controls.
	 */
	$wp_customize->add_setting( 'layout_width', array(
		'default'           => 'medium',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'layout_width', array(
		'label'   => esc_html__( 'Width', 'runway' ),
		'section' => 'layout',
		'type'    => 'radio',
		'choices' => array(
			'small'  => esc_html__( 'Small', 'runway' ),
			'medium' => esc_html__( 'Medium', 'runway' ),
			'large'  => esc_html__( 'Large', 'runway' ),
		),
	) );

	/**
	 * Look & Feel section settings and controls.
	 */
	$wp_customize->add_setting( 'title_text_transform', array(
		'default'           => 'capitalize',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'title_text_transform', array(
		'choices' => array(
			'none'                    => esc_html__( 'None (Text renders as it is written by author)', 'runway' ),
			'capitalize-first-letter' => esc_html__( 'Capitalize First Letter', 'runway' ),
			'capitalize'              => esc_html__( 'Capitalize', 'runway' ),
			'uppercase'               => esc_html__( 'Uppercase', 'runway' ),
			'lowercase'               => esc_html__( 'Lowercase', 'runway' ),
		),
		'label'   => esc_html__( 'Title Text Transform ', 'runway' ),
		'section' => 'look_and_feel',
		'type'    => 'radio',
	) );

	$wp_customize->add_setting( 'title_highlight_style', array(
		'default'           => 'none',
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'title_highlight_style', array(
		'choices' => array(
			'none'           => esc_html__( 'None', 'runway' ),
			'accent-bg-half' => esc_html__( 'Half Accent Background', 'runway' ),
		),
		'label'   => esc_html__( 'Title Highlight Style', 'runway' ),
		'section' => 'look_and_feel',
		'type'    => 'radio',
	) );

	/**
	 * Typography section settings and controls.
	 */
	$wp_customize->add_setting( 'typography_base_font_size', array(
		'default'           => 16,
		'sanitize_callback' => 'absint',
	) );
	$wp_customize->add_control( 'typography_base_font_size', array(
		'label'   => esc_html__( 'Base Font Size', 'runway' ),
		'section' => 'typography',
		'type'    => 'select',
		'choices' => array(
			10 => '10',
			11 => '11',
			12 => '12',
			13 => '13',
			14 => '14',
			15 => '15',
			16 => '16',
			17 => '17',
			18 => '18',
			19 => '19',
			20 => '20',
		),
	) );

	$wp_customize->add_setting( 'typography_mobile_base_font_size', array(
		'default'           => 12,
		'sanitize_callback' => 'absint',
	) );
	$wp_customize->add_control( 'typography_mobile_base_font_size', array(
		'label'   => esc_html__( 'Mobile Base Font Size', 'runway' ),
		'section' => 'typography',
		'type'    => 'select',
		'choices' => array(
			10 => '10',
			11 => '11',
			12 => '12',
			13 => '13',
			14 => '14',
			15 => '15',
			16 => '16',
			17 => '17',
			18 => '18',
			19 => '19',
			20 => '20',
		),
	) );

	/**
	 * Scocial Media section settings and controls.
	 */
	$wp_customize->add_setting( 'behance_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'behance_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://www.behance.net/sbelsky' ),
		'label'       => esc_html__( 'Behance', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'bloglovin_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'bloglovin_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://www.bloglovin.com/@dan' ),
		'label'       => esc_html__( 'Bloglovin', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'dribbble_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'dribbble_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://dribbble.com/simplebits' ),
		'label'       => esc_html__( 'Dribbble', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'facebook_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'facebook_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://www.facebook.com/zuck' ),
		'label'       => esc_html__( 'Facebook', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'flattr_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'flattr_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://flattr.com/@peter' ),
		'label'       => esc_html__( 'Flattr', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'flickr_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'flickr_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://www.flickr.com/photos/stewart' ),
		'label'       => esc_html__( 'Flickr', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'github_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'github_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://github.com/defunkt' ),
		'label'       => esc_html__( 'GitHub', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'gitlab_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'gitlab_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://gitlab.com/sytses' ),
		'label'       => esc_html__( 'GitLab', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'googleplus_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'googleplus_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), '<br>https://plus.google.com/116899029375914044550<br>https://plus.google.com/+google' ),
		'label'       => esc_html__( 'Google+', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'instagram_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'instagram_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://www.instagram.com/kevin/' ),
		'label'       => esc_html__( 'Instagram', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'linkedin_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'linkedin_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://www.linkedin.com/in/jeffweiner08/' ),
		'label'       => esc_html__( 'LinkedIn', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'medium_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'medium_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://medium.com/@ev' ),
		'label'       => esc_html__( 'Medium', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'pinterest_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'pinterest_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://www.pinterest.com/pinterest/' ),
		'label'       => esc_html__( 'Pinterest', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'snapchat_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'snapchat_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://www.snapchat.com/add/evan' ),
		'label'       => esc_html__( 'Snapchat', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'soundcloud_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'soundcloud_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://soundcloud.com/alex' ),
		'label'       => esc_html__( 'SoundCloud', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'tumblr_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'tumblr_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'http://david.tumblr.com' ),
		'label'       => esc_html__( 'Tumblr', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'twitch_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'twitch_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://www.twitch.tv/twitch' ),
		'label'       => esc_html__( 'Twitch', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'twitter_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'twitter_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://twitter.com/Twitter' ),
		'label'       => esc_html__( 'Twitter', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'vimeo_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'vimeo_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), 'https://vimeo.com/staffpicks' ),
		'label'       => esc_html__( 'Vimeo', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'youtube_url', array(
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( 'youtube_url', array(
		/* translators: %s: Example URL. */
		'description' => sprintf( esc_html__( 'Example: %s', 'runway' ), '<br>https://www.youtube.com/user/YouTube<br>https://www.youtube.com/channel/UCBR8-60-B28hp2BmDPdntcQ' ),
		'label'       => esc_html__( 'YouTube', 'runway' ),
		'section'     => 'social_links',
		'type'        => 'text',
	) );

	$wp_customize->add_setting( 'social_sharing', array(
		'default'           => array( 'facebook', 'twitter', 'googleplus' ),
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( new Runway_Checkbox_Multiple_Control( $wp_customize, 'social_sharing', array(
		'label'   => esc_html__( 'Show Buttons For', 'runway' ),
		'section' => 'social_sharing',
		'choices' => array(
			'buffer'      => esc_html__( 'Buffer', 'runway' ),
			'digg'        => esc_html__( 'Digg', 'runway' ),
			'email'       => esc_html__( 'Email', 'runway' ),
			'facebook'    => esc_html__( 'Facebook', 'runway' ),
			'flattr'      => esc_html__( 'Flattr', 'runway' ),
			'googleplus'  => esc_html__( 'Google+', 'runway' ),
			'linkedin'    => esc_html__( 'LinkedIn', 'runway' ),
			'pinterest'   => esc_html__( 'Pinterest', 'runway' ),
			'pocket'      => esc_html__( 'Pocket', 'runway' ),
			'reddit'      => esc_html__( 'Reddit', 'runway' ),
			'stumbleupon' => esc_html__( 'StumbleUpon', 'runway' ),
			'tumblr'      => esc_html__( 'Tumblr', 'runway' ),
			'twitter'     => esc_html__( 'Twitter', 'runway' ),
			'vkontakte'   => esc_html__( 'Vkontakte', 'runway' ),
			'whatsapp'    => esc_html__( 'WhatsApp', 'runway' ),
			'xing'        => esc_html__( 'Xing', 'runway' ),
		),
	) ) );

	$wp_customize->add_setting( 'twitter_id', array(
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'twitter_id', array(
		'description'     => esc_html__( 'Set this if you want to add "via @username" after share tweet.', 'runway' ),
		'label'           => esc_html__( 'Twitter Username', 'runway' ),
		'section'         => 'social_sharing',
		'type'            => 'text',
		'active_callback' => function () {
			$social_sharing = get_theme_mod( 'social_sharing', array( 'facebook', 'twitter', 'googleplus' ) );
			if ( is_string( $social_sharing ) && false !== strpos( $social_sharing, 'twitter' ) ) {
				return true;
			} elseif ( is_array( $social_sharing ) && isset( $social_sharing['twitter'] ) ) {
				return true;
			}
			return false;
		},
	) );

	/**
	 * Footer section settings and controls.
	 */
	$wp_customize->add_setting( 'footer_bg_transparent', array(
		'default'           => false,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'footer_bg_transparent', array(
		'label'   => esc_html__( 'Transparent Background Color', 'runway' ),
		'section' => 'footer',
		'type'    => 'checkbox',
	) );

	$wp_customize->add_setting( 'footer_bg_color', array(
		'default'           => '#212121',
		'sanitize_callback' => 'sanitize_hex_color',
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_bg_color', array(
		'label'           => esc_html__( 'Background Color', 'runway' ),
		'section'         => 'footer',
		'active_callback' => function () {
			return get_theme_mod( 'footer_bg_transparent', true ) ? false : true;
		},
	) ) );

	$wp_customize->add_setting( 'footer_text_colors', array(
		'default'           => 'auto',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'footer_text_colors', array(
		'label'   => esc_html__( 'Text Colors', 'runway' ),
		'section' => 'footer',
		'type'    => 'radio',
		'choices' => array(
			'auto'  => esc_html__( 'Auto', 'runway' ),
			'light' => esc_html__( 'Light', 'runway' ),
			'dark'  => esc_html__( 'Dark', 'runway' ),
		),
	) );

	$wp_customize->add_setting( 'footer_full_width', array(
		'default'           => true,
		'sanitize_callback' => 'runway_sanitize_checkbox',
	) );
	$wp_customize->add_control( 'footer_full_width', array(
		'label'           => esc_html__( 'Full Width', 'runway' ),
		'section'         => 'footer',
		'type'            => 'checkbox',
		'active_callback' => function () {
			return get_theme_mod( 'footer_bg_transparent', true ) ? false : true;
		},
	) );

	$wp_customize->add_setting( 'footer_content_wrapper', array(
		'default'           => 'medium',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'footer_content_wrapper', array(
		'label'   => esc_html__( 'Content Wrapper Width', 'runway' ),
		'section' => 'footer',
		'type'    => 'select',
		'choices' => array(
			'small'  => esc_html__( 'Small', 'runway' ),
			'medium' => esc_html__( 'Medium', 'runway' ),
			'large'  => esc_html__( 'Large', 'runway' ),
			'full'   => esc_html__( 'Full', 'runway' ),
		),
	) );

	$wp_customize->add_setting( 'footer_columns', array(
		'default'           => 3,
		'sanitize_callback' => 'absint',
	) );
	$wp_customize->add_control( 'footer_columns', array(
		'label'   => esc_html__( 'Widget Columns', 'runway' ),
		'section' => 'footer',
		'type'    => 'select',
		'choices' => array(
			0 => esc_html__( 'None', 'runway' ),
			1 => esc_html__( '1', 'runway' ),
			2 => esc_html__( '2', 'runway' ),
			3 => esc_html__( '3', 'runway' ),
			4 => esc_html__( '4', 'runway' ),
			5 => esc_html__( '5', 'runway' ),
			6 => esc_html__( '6', 'runway' ),
		),
	) );

	$column_widths = array(
		'one-sixth'      => esc_html__( '16.67%', 'runway' ),
		'one-quarter'    => esc_html__( '25%', 'runway' ),
		'one-third'      => esc_html__( '33.33%', 'runway' ),
		'one-half'       => esc_html__( '50%', 'runway' ),
		'two-thirds'     => esc_html__( '66.67%', 'runway' ),
		'three-quarters' => esc_html__( '75%', 'runway' ),
		'five-sixths'    => esc_html__( '83.33%', 'runway' ),
		'one-whole'      => esc_html__( '100%', 'runway' ),
	);

	$wp_customize->add_setting( 'footer_column_1_width', array(
		'default'           => 'two-sixths',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'footer_column_1_width', array(
		'label'           => esc_html__( 'Column 1 Width', 'runway' ),
		'section'         => 'footer',
		'type'            => 'select',
		'choices'         => $column_widths,
		'active_callback' => function () {
			return 1 <= get_theme_mod( 'footer_columns', 3 ) ? true : false;
		},
	) );

	$wp_customize->add_setting( 'footer_column_2_width', array(
		'default'           => 'two-sixths',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'footer_column_2_width', array(
		'label'           => esc_html__( 'Column 2 Width', 'runway' ),
		'section'         => 'footer',
		'type'            => 'select',
		'choices'         => $column_widths,
		'active_callback' => function () {
			return 2 <= get_theme_mod( 'footer_columns', 3 ) ? true : false;
		},
	) );

	$wp_customize->add_setting( 'footer_column_3_width', array(
		'default'           => 'two-sixths',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'footer_column_3_width', array(
		'label'           => esc_html__( 'Column 3 Width', 'runway' ),
		'section'         => 'footer',
		'type'            => 'select',
		'choices'         => $column_widths,
		'active_callback' => function () {
			return 3 <= get_theme_mod( 'footer_columns', 3 ) ? true : false;
		},
	) );

	$wp_customize->add_setting( 'footer_column_4_width', array(
		'default'           => 'two-sixths',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'footer_column_4_width', array(
		'label'           => esc_html__( 'Column 4 Width', 'runway' ),
		'section'         => 'footer',
		'type'            => 'select',
		'choices'         => $column_widths,
		'active_callback' => function () {
			return 4 <= get_theme_mod( 'footer_columns', 3 ) ? true : false;
		},
	) );

	$wp_customize->add_setting( 'footer_column_5_width', array(
		'default'           => 'two-sixths',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'footer_column_5_width', array(
		'label'           => esc_html__( 'Column 5 Width', 'runway' ),
		'section'         => 'footer',
		'type'            => 'select',
		'choices'         => $column_widths,
		'active_callback' => function () {
			return 5 <= get_theme_mod( 'footer_columns', 3 ) ? true : false;
		},
	) );

	$wp_customize->add_setting( 'footer_column_6_width', array(
		'default'           => 'two-sixths',
		'sanitize_callback' => 'sanitize_key',
	) );
	$wp_customize->add_control( 'footer_column_6_width', array(
		'label'           => esc_html__( 'Column 6 Width', 'runway' ),
		'section'         => 'footer',
		'type'            => 'select',
		'choices'         => $column_widths,
		'active_callback' => function () {
			return 6 <= get_theme_mod( 'footer_columns', 3 ) ? true : false;
		},
	) );

}
add_action( 'customize_register', 'runway_customize_register' );

/**
 * Enqueue customize CSS.
 */
function runway_customize_css() {
	$max = get_theme_mod( 'typography_base_font_size', 16 );
	$min = get_theme_mod( 'typography_mobile_base_font_size', 12 );
	?>
	<style type="text/css"></style>
	<?php
}
add_action( 'wp_head', 'runway_customize_css' );

/**
 * Change the mobile and tablet preview dimensions in the WP Customizer.
 */
function runway_customizer_responsive_sizes() {
	?>
	<style>
	.wp-customizer .preview-mobile .wp-full-overlay-main {
		margin-left: 0;
		width: 479px;
		height: 720px;
		max-height: 100vh;
		transform: translateX(-50%);
	}
	.wp-customizer .preview-tablet .wp-full-overlay-main {
		margin-left: 0;
		width: 839px;
		height: 1260px;
		max-height: 100vh;
		transform: translateX(-50%);
	}
	</style>
	<?php
}
add_action( 'customize_controls_print_styles', 'runway_customizer_responsive_sizes' );
