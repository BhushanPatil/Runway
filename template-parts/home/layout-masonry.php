<?php
/**
 * Template part for displaying blog posts in masonry style layout.
 *
 * @package Runway
 */

$full_width_first_post = get_theme_mod( 'homepage_full_width_first_post', false );
$title_text_transform  = get_theme_mod( 'title_text_transform', 'capitalize' );

$title_classes = 'entry-teaser__title';
if ( 'none' !== $title_text_transform ) {
	$title_classes .= ' text-transform-' . $title_text_transform;
}

if ( have_posts() ) : ?>
<div class="masonry">
	<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>
	<?php
	$permalink   = get_permalink();
	$the_excerpt = get_the_excerpt();
	?>
	<?php if ( $full_width_first_post && 0 === $wp_query->current_post && ! is_paged() ) : ?>
	<div class="masonry__item masonry__item--width-full">
	<?php else : ?>
	<div class="masonry__item">
	<?php endif; ?>
		<article <?php runway_post_class( 'entry-teaser' ); ?>>
			<header class="entry-teaser__header">
				<?php if ( has_post_thumbnail() ) : ?>
				<div class="entry-teaser__thumbnail">
					<a class="entry-teaser__thumbnail-link" href="<?php echo esc_url( $permalink ); ?>">
					<?php
					the_post_thumbnail(
						array( 528, 528 ),
						array(
							'class' => 'entry-teaser__thumbnail-image',
						)
					);
					?>
					</a>
				</div>
				<?php endif; ?>
				<h2 class="<?php echo esc_attr( $title_classes ); ?>">
					<?php the_title( sprintf( '<a class="entry-teaser__title-link" href="%s" rel="bookmark">', $permalink ), '</a>' ); ?>
				</h2>
			</header>
			<?php if ( ! empty( trim( $the_excerpt ) ) ) : ?>
			<div class="entry-teaser__content"><?php echo $the_excerpt; // WPCS: XSS ok. ?></div>
			<?php endif; ?>
			<footer class="entry-teaser__footer">
				<?php get_template_part( 'template-parts/metadata/teaser' ); ?>
			</footer>
		</article>
	</div>
	<?php endwhile; ?>
</div>
<?php endif; ?>
