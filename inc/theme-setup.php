<?php
/**
 * Theme setup functions and definitions
 *
 * @package Runway
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 */
function runway_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 */
	load_theme_textdomain( 'runway', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'header-primary'   => esc_html__( 'Header Primary', 'runway' ),
		'header-secondary' => esc_html__( 'Header Secondary', 'runway' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
}
add_action( 'after_setup_theme', 'runway_setup' );

/**
 * Enqueue scripts and styles.
 */
function runway_scripts() {

	// Don't enqueue on the backend.
	if ( is_admin() ) {
		return;
	}

	// Enqueue scripts.
	wp_enqueue_script(
		'runway-html5shiv-js',
		get_theme_file_uri( '/assets/js/html5shiv.min.js' ),
		array(),
		'3.7.3'
	);
	wp_script_add_data(
		'runway-html5shiv-js',
		'conditional',
		'lt IE 9'
	);

	wp_enqueue_script(
		'runway-cssua-js',
		get_theme_file_uri( '/assets/js/cssua.min.js' ),
		array(),
		'2.1.31',
		true
	);

	wp_enqueue_script(
		'runway-jquery-bez-js',
		get_theme_file_uri( '/assets/js/jquery.bez.min.js' ),
		array( 'jquery' ),
		'1.0.11',
		true
	);

	wp_enqueue_script(
		'runway-slick-js',
		get_theme_file_uri( '/assets/js/slick.min.js' ),
		array( 'jquery' ),
		'1.8.0',
		true
	);

	wp_enqueue_script(
		'runway-masonry-js',
		get_theme_file_uri( '/assets/js/masonry.pkgd.min.js' ),
		array( 'jquery' ),
		'4.2.0',
		true
	);

	wp_enqueue_script(
		'runway-main-js',
		get_theme_file_uri( '/assets/js/runway.js' ),
		array( 'jquery' ),
		null,
		true
	);

	// Enqueue theme stylesheet.
	wp_enqueue_style(
		'runway-main-css',
		get_stylesheet_uri(),
		array(),
		null
	);

	// Enqueue default font stylesheet.
	wp_enqueue_style(
		'runway-fonts',
		'https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,600i,700,700i|Roboto+Mono:400,400i,500,500i|Roboto:300,300i,400,400i,500,500i,700,700i',
		array(),
		null
	);
}
add_action( 'wp_enqueue_scripts', 'runway_scripts' );

/**
 * Register sidebars.
 */
function runway_sidebars() {
	register_sidebar( array(
		'id'            => 'sidebar',
		'name'          => esc_html__( 'Homepage', 'runway' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s" aria-labelledby="widget-title">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 id="widget-title" class="widget-title"><span class="widget-title-text">',
		'after_title'   => '</span></h3>',
	) );
	register_sidebar( array(
		'id'            => 'sidebar-post',
		'name'          => esc_html__( 'Post Sidebar', 'runway' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s" aria-labelledby="widget-title">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 id="widget-title" class="widget-title"><span class="widget-title-text">',
		'after_title'   => '</span></h3>',
	) );
	register_sidebar( array(
		'id'            => 'sidebar-page',
		'name'          => esc_html__( 'Page Sidebar', 'runway' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s" aria-labelledby="widget-title">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 id="widget-title" class="widget-title"><span class="widget-title-text">',
		'after_title'   => '</span></h3>',
	) );
	for ( $i = 1; $i <= 6; $i++ ) {
		register_sidebar( array(
			'id'            => 'footer-' . $i,
			/* translators: %s: Footer column number */
			'name'          => sprintf( esc_html__( 'Footer Column %s', 'runway' ), $i ),
			'before_widget' => '<section id="%1$s" class="widget %2$s" aria-labelledby="widget-title">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 id="widget-title" class="widget-title"><span class="widget-title-text">',
			'after_title'   => '</span></h3>',
		) );
	}
}
add_action( 'widgets_init', 'runway_sidebars' );
