<?php
/**
 * Template part for displaying blog posts in masonry style layout.
 *
 * @package Runway
 */

?>
<?php
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		$permalink   = get_permalink();
		$the_excerpt = get_the_excerpt();
		?>
		<article <?php runway_post_class( 'entry-teaser' ); ?>>
			<header class="entry-teaser__header">
				<?php if ( has_post_thumbnail() ) : ?>
				<div class="entry-teaser__thumbnail">
					<a class="entry-teaser__thumbnail-link" href="<?php echo esc_url( $permalink ); ?>">
					<?php
					the_post_thumbnail(
						array( 528, 528 ),
						array(
							'class' => 'entry-teaser__thumbnail-image',
						)
					);
					?>
					</a>
				</div>
				<?php else : ?>
				<div class="entry-teaser__thumbnail">
					<a class="entry-teaser__thumbnail-link" href="<?php echo esc_url( $permalink ); ?>">
						<img class="entry-teaser__thumbnail-image" src="http://via.placeholder.com/528x528" alt="thumbnail">
					</a>
				</div>
				<?php endif; ?>
				<h2 class="entry-teaser__title">
					<?php the_title( sprintf( '<a class="entry-teaser__title-link" href="%s" rel="bookmark">', $permalink ), '</a>' ); ?>
				</h2>
			</header>
			<?php if ( ! empty( trim( $the_excerpt ) ) ) : ?>
			<div class="entry-teaser__content"><?php echo $the_excerpt; // WPCS: XSS ok. ?></div>
			<?php endif; ?>
			<footer class="entry-teaser__footer">
				<?php get_template_part( 'template-parts/metadata/teaser' ); ?>
			</footer>
		</article>
		<?php
	endwhile;
endif;
