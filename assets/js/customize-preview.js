/**
 * File customize-preview.js.
 *
 * Instantly live-update customizer settings in the preview for improved user experience.
 */
( function( $ ) {
	wp.customize( 'header_bg_color', function( value ) {
		value.bind( function( to ) {
			$( '.hero__bg' ).css( 'background-color', to );
		});
	});
} ( jQuery ) );
