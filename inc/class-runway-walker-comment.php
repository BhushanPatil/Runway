<?php
/**
 * Runway_Walker_Comment class
 *
 * @package Runway
 */

/**
 * Walker class used to create an HTML list of comments.
 */
class Runway_Walker_Comment extends Walker_Comment {

	/**
	 * BEM parent block class name.
	 *
	 * @var string
	 */
	public $parent_class = '';

	/**
	 * BEM comment block class name.
	 *
	 * @var string
	 */
	public $comment_class = '';

	/**
	 * Constructor method to set BEM block class name.
	 *
	 * @param string $parent_class BEM parent block class name.
	 * @param string $comment_class BEM comment block class name.
	 */
	public function __construct( $parent_class = 'comments-list', $comment_class = 'comment' ) {
		$this->parent_class  = false === strpos( $parent_class, '__' ) ? $parent_class . '__' : $parent_class . '-';
		$this->comment_class = $comment_class;
	}

	/**
	 * Starts the list before the elements are added.
	 *
	 * @see Walker::start_lvl()
	 * @global int $comment_depth
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Optional. Depth of the current comment. Default 0.
	 * @param array  $args   Optional. Uses 'style' argument for type of HTML list. Default empty array.
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$GLOBALS['comment_depth'] = $depth++; // WPCS: override ok.
		switch ( $args['style'] ) {
			case 'div':
				break;
			case 'ol':
				$output .= '<ol class="' . $this->parent_class . 'children">' . "\n";
				break;
			case 'ul':
			default:
				$output .= '<ul class="' . $this->parent_class . 'children">' . "\n";
				break;
		}
	}

	/**
	 * Ends the list of items after the elements are added.
	 *
	 * @see Walker::end_lvl()
	 * @global int $comment_depth
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Optional. Depth of the current comment. Default 0.
	 * @param array  $args   Optional. Will only append content if style argument value is 'ol' or 'ul'.
	 *                       Default empty array.
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$GLOBALS['comment_depth'] = $depth++; // WPCS: override ok.

		switch ( $args['style'] ) {
			case 'div':
				break;
			case 'ol':
				$output .= "</ol>\n";
				break;
			case 'ul':
			default:
				$output .= "</ul>\n";
				break;
		}
	}

	/**
	 * Starts the element output.
	 *
	 * @global int        $comment_depth
	 * @global WP_Comment $comment
	 *
	 * @param string     $output  Used to append additional content. Passed by reference.
	 * @param WP_Comment $comment Comment data object.
	 * @param int        $depth   Optional. Depth of the current comment in reference to parents. Default 0.
	 * @param array      $args    Optional. An array of arguments. Default empty array.
	 * @param int        $id      Optional. ID of the current comment. Default 0 (unused).
	 */
	public function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
		$depth++;
		$GLOBALS['comment_depth'] = $depth; // WPCS: override ok.
		$GLOBALS['comment']       = $comment; // WPCS: override ok.

		if ( ! empty( $args['callback'] ) ) {
			ob_start();
			call_user_func( $args['callback'], $comment, $args, $depth );
			$output .= ob_get_clean();
			return;
		}

		if ( ( 'pingback' === $comment->comment_type || 'trackback' === $comment->comment_type ) && $args['short_ping'] ) {
			ob_start();
			$this->ping( $comment, $depth, $args );
			$output .= ob_get_clean();
		} else {
			ob_start();
			$this->html5_comment( $comment, $depth, $args );
			$output .= ob_get_clean();
		}
	}

	/**
	 * Ends the element output, if needed.
	 *
	 * @param string     $output  Used to append additional content. Passed by reference.
	 * @param WP_Comment $comment The current comment object. Default current comment.
	 * @param int        $depth   Optional. Depth of the current comment. Default 0.
	 * @param array      $args    Optional. An array of arguments. Default empty array.
	 */
	public function end_el( &$output, $comment, $depth = 0, $args = array() ) {
		if ( ! empty( $args['end-callback'] ) ) {
			ob_start();
			call_user_func( $args['end-callback'], $comment, $args, $depth );
			$output .= ob_get_clean();
			return;
		}
		if ( 'div' === $args['style'] ) {
			$output .= "</div>\n";
		} else {
			$output .= "</li>\n";
		}
	}

	/**
	 * Outputs a comment in the HTML5 format.
	 *
	 * @param WP_Comment $comment Comment to display.
	 * @param int        $depth   Depth of the current comment.
	 * @param array      $args    An array of arguments.
	 */
	protected function html5_comment( $comment, $depth, $args ) {
		$tag = ( 'div' === $args['style'] ) ? 'div' : 'li';
		echo '<' . $tag . ' id="comment- ' . get_comment_ID() . '" class="' . $this->parent_class . 'item">' . "\n"; // WPCS: XSS ok.
		?>
		<article <?php $this->comment_class(); ?>>
			<header class="<?php echo $this->comment_class; // WPCS: XSS ok. ?>__header">
				<div class="<?php echo $this->comment_class; // WPCS: XSS ok. ?>__author">
					<?php
					$url    = get_comment_author_url( $comment );
					$author = get_comment_author( $comment );
					if ( 0 !== $args['avatar_size'] ) {
						echo get_avatar(
							$comment,
							$args['avatar_size'],
							null,
							sprintf(
								/* translators: %s: Post author display name or username, i.e. John Smith, admin. */
								__( 'Profile picture of comment author "%s"', 'runway' ),
								$author
							),
							array(
								'class' => $this->comment_class . '__author-image-tag',
							)
						);
					}
					if ( empty( $url ) || 'http://' === $url ) {
						$author_name = $author;
					} else {
						$author_name = '<a class="' . $this->comment_class . '__author-name-link" href="' . esc_url( $url ) . '" rel="external nofollow">' . esc_html( $author ) . '</a>';
					}
					printf( '<b class="' . $this->comment_class . '__author-name">%s</b>', $author_name ); // WPCS:XSS ok.
					?>
				</div>
				<a class="<?php echo $this->comment_class; // WPCS: XSS ok. ?>__datetime-link" href="<?php echo esc_url( get_comment_link( $comment, $args ) ); ?>">
					<time datetime="<?php comment_time( 'c' ); ?>">
						<?php
							// translators: %s: Difference between current time and comment time, i.e. 1 day, 3 weeks.
							printf( __( '%s ago', 'runway' ), human_time_diff( get_comment_time( 'U' ), current_time( 'timestamp' ) ) ); // WPCS:XSS ok.
						?>
					</time>
				</a>
				<?php
				add_filter( 'edit_comment_link', function ( $class ) {
					return str_replace( 'class="comment-edit-link"', 'class="' . $this->comment_class . '__edit-link"', $class );
				});
				edit_comment_link( __( 'Edit', 'runway' ), '<span class="' . $this->comment_class . '__edit">', '</span>' );
				?>
				<?php if ( '0' === $comment->comment_approved ) : ?>
				<p class="<?php echo $this->comment_class; // WPCS: XSS ok. ?>__awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'runway' ); ?></p>
				<?php endif; ?>
			</header>
			<div class="<?php echo $this->comment_class; // WPCS: XSS ok. ?>__content">
				<?php comment_text(); ?>
			</div>
			<?php
			add_filter( 'comment_reply_link', function ( $class ) {
				return str_replace( "class='comment-reply-link", "class='" . $this->comment_class . '__reply-link', $class );
			});
			comment_reply_link( array_merge( $args, array(
				'add_below' => 'div-comment',
				'depth'     => $depth,
				'max_depth' => $args['max_depth'],
				'before'    => '<footer class="' . $this->comment_class . '__footer"><div class="' . $this->comment_class . '__reply">',
				'after'     => '</div></footer>',
			) ) );
			?>
		</article>
		<?php
	}

	/**
	 * Generates semantic classes for each comment element.
	 *
	 * @param string|array   $class    Optional. One or more classes to add to the class list.
	 *                                 Default empty.
	 * @param int|WP_Comment $comment  Comment ID or WP_Comment object. Default current comment.
	 * @param int|WP_Post    $post_id  Post ID or WP_Post object. Default current post.
	 * @param bool           $echo     Optional. Whether to cho or return the output.
	 *                                 Default true.
	 * @return string If `$echo` is false, the class will be returned. Void otherwise.
	 */
	protected function comment_class( $class = '', $comment = null, $post_id = null, $echo = true ) {

		$classes_array = get_comment_class( $class, $comment, $post_id );

		if ( 'comment' !== $this->comment_class ) {
			$classes_array[0] = $this->comment_class;
		}

		$classes = join( ' ', $classes_array );

		$classes = str_replace( ' byuser', ' ' . $this->comment_class . '--by-user', $classes );

		$classes = str_replace( ' comment-author-', ' ' . $this->comment_class . '--author-', $classes );

		$classes = str_replace( ' bypostauthor', ' ' . $this->comment_class . '--by-post-author', $classes );

		$classes = str_replace( ' odd', ' ' . $this->comment_class . '--odd', $classes );

		$classes = str_replace( ' alt', ' ' . $this->comment_class . '--alt', $classes );

		$classes = str_replace( ' even', ' ' . $this->comment_class . '--even', $classes );

		$classes = str_replace( ' thread-odd', ' ' . $this->comment_class . '--thread-odd', $classes );

		$classes = str_replace( ' thread-alt', ' ' . $this->comment_class . '--thread-alt', $classes );

		$classes = str_replace( ' thread-even', ' ' . $this->comment_class . '--thread-even', $classes );

		$classes = str_replace( ' depth-', ' ' . $this->comment_class . '--depth-', $classes );

		if ( $this->has_children ) {
			$classes .= ' ' . $this->comment_class . '--parent';
		}

		// Separates classes with a single space, collates classes for comment DIV.
		$class = 'class="' . $classes . '"';
		if ( $echo ) {
			echo $class; // WPCS:XSS ok.
		} else {
			return $class;
		}
	}
}
