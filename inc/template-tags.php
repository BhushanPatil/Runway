<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Runway
 */

/**
 * Prints HTML for social links.
 *
 * @param array|string $social_links Selected social links.
 * @param string       $classes      CSS classes, first class name will be BEM block.
 */
function runway_social_links( $social_links, $classes = 'social-links' ) {

	// Return if no site is selected.
	if ( empty( $social_links ) ) {
		return;
	}

	// Convert string stored in database to array.
	if ( is_string( $social_links ) ) {
		$social_links = explode( ',', $social_links );
	}

	// Get block class name.
	$block = explode( ' ', $classes )[0];

	if ( strpos( $block, '__' ) !== false ) {
		$block = $block . '-';
	} else {
		$block = $block . '__';
	}

	// Output HTML markup.
	echo '<ul class="' . esc_attr( $classes ) . '">';
	foreach ( $social_links as $service ) {
		$url = '';
		if ( 'rss' === $service ) {
			$url = get_bloginfo( 'rss2_url' );
		} else {
			$url = get_theme_mod( $service . '_url' );
		}
		if ( empty( $url ) ) {
			continue;
		}

		$li_classes = $block . 'item';

		$a_classes  = $block . 'link';
		$a_classes .= ' ' . $block . 'link--' . $service;

		$i_classes  = 'socicon-' . $service;
		$i_classes .= ' ' . $block . 'icon';
		?>
		<li class="<?php echo esc_attr( $li_classes ); ?>">
			<a class="<?php echo esc_attr( $a_classes ); ?>" href="<?php echo esc_url( $url ); ?>" target="_blank"><i class="<?php echo esc_attr( $i_classes ); ?>"></i></a>
		</li>
		<?php
	}
	echo '</ul>';
}

/**
 * Prints HTML for social share links.
 *
 * @param array|string $services Selected services.
 */
function runway_social_sharing( $services ) {

	// Return if no site is selected.
	if ( empty( $services ) ) {
		return;
	}

	// Convert string stored in database to array.
	if ( is_string( $services ) ) {
		$services = explode( ',', $services );
	}

	// Get current page URL and encode to use in a query part of share URL.
	$url = rawurlencode( get_permalink() );

	// Get current page title and encode to use in a query part of share URL.
	$title = rawurlencode( get_the_title() );

	// Get Post Thumbnail for pinterest.
	$media = rawurldecode( wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0] );

	// Twitter via variable.
	$via        = '';
	$twitter_id = get_theme_mod( 'twitter_id' );
	if ( ! empty( $twitter_id ) ) {
		$via = '&via=' . $twitter_id;
	}

	// Services data.
	$services_data = array(
		'buffer'      => array(
			'name'  => __( 'Buffer', 'runway' ),
			'text'  => __( 'Share on Buffer', 'runway' ),
			'href'  => 'https://bufferapp.com/add?url=${url}&text=${title}',
			'icon'  => 'buffer',
			'color' => '#168EEA',
		),
		'digg'        => array(
			'name'  => __( 'Digg', 'runway' ),
			'text'  => __( 'Share on Digg', 'runway' ),
			'href'  => 'http://www.digg.com/submit?url=${url}',
			'icon'  => 'digg',
			'color' => '#1D1D1B',
		),
		'email'       => array(
			'name'  => __( 'Email', 'runway' ),
			'text'  => __( 'Share by Email', 'runway' ),
			'href'  => 'mailto:?subject=body=${title}%0A${url}',
			'icon'  => 'mail',
			'color' => '#9E9E9E',
		),
		'facebook'    => array(
			'name'  => __( 'Facebook', 'runway' ),
			'text'  => __( 'Share on Facebook', 'runway' ),
			'href'  => 'https://www.facebook.com/sharer/sharer.php?u=${url}',
			'icon'  => 'facebook',
			'color' => '#3B5998',
		),
		'flattr'      => array(
			'name'  => __( 'Flattr', 'runway' ),
			'text'  => __( 'Share on Flattr', 'runway' ),
			'href'  => 'https://flattr.com/submit/auto?url=${url}&title=${title}',
			'icon'  => 'flattr',
			'color' => '#F67C1A',
		),
		'googleplus'  => array(
			'name'  => __( 'Google+', 'runway' ),
			'text'  => __( 'Share on Google+', 'runway' ),
			'href'  => 'https://plus.google.com/share?url=${url}',
			'icon'  => 'googleplus',
			'color' => '#DD4B39',
		),
		'linkedin'    => array(
			'name'  => __( 'LinkedIn', 'runway' ),
			'text'  => __( 'Share on LinkedIn', 'runway' ),
			'href'  => 'https://www.linkedin.com/shareArticle?mini=true&url=${url}&title=${title}',
			'icon'  => 'linkedin',
			'color' => '#0077B5',
		),
		'pinterest'   => array(
			'name'  => __( 'Pinterest', 'runway' ),
			'text'  => __( 'Share on Pinterest', 'runway' ),
			'href'  => 'https://pinterest.com/pin/create/button/?url=${url}&description=${title}&media=${media}',
			'icon'  => 'pinterest',
			'color' => '#BD081C',
		),
		'pocket'      => array(
			'name'  => __( 'Pocket', 'runway' ),
			'text'  => __( 'Save on Pocket', 'runway' ),
			'href'  => 'https://getpocket.com/save?url=${url}&title=${title}',
			'icon'  => 'pocket',
			'color' => '#EF4056',
		),
		'reddit'      => array(
			'name'  => __( 'Reddit', 'runway' ),
			'text'  => __( 'Share on Reddit', 'runway' ),
			'href'  => 'https://www.reddit.com/submit?url=${url}&title=${title}',
			'icon'  => 'reddit',
			'color' => '#FF4500',
		),
		'stumbleupon' => array(
			'name'  => __( 'StumbleUpon', 'runway' ),
			'text'  => __( 'Share on StumbleUpon', 'runway' ),
			'href'  => 'https://www.stumbleupon.com/submit?url=${url}&title=${title}',
			'icon'  => 'stumbleupon',
			'color' => '#EB4823',
		),
		'tumblr'      => array(
			'name'  => __( 'Tumblr', 'runway' ),
			'text'  => __( 'Share on Tumblr', 'runway' ),
			'href'  => 'http://www.tumblr.com/share/link?url=${url}',
			'icon'  => 'tumblr',
			'color' => '#32506D',
		),
		'twitter'     => array(
			'name'  => __( 'Twitter', 'runway' ),
			'text'  => __( 'Share on Twitter', 'runway' ),
			'href'  => 'https://twitter.com/intent/tweet?url=${url}&text=${title}' . ( get_theme_mod( 'twitter' ) ? '&via=' . get_theme_mod( 'twitter' ) : '' ),
			'icon'  => 'twitter',
			'color' => '#1DA1F2',
		),
		'vkontakte'   => array(
			'name'  => __( 'VKontakte', 'runway' ),
			'text'  => __( 'Share on VKontakte', 'runway' ),
			'href'  => 'https://vkontakte.ru/share.php?url=${url}',
			'icon'  => 'vkontakte',
			'color' => '#45668E',
		),
		'whatsapp'    => array(
			'name'  => __( 'WhatsApp', 'runway' ),
			'text'  => __( 'Share on WhatsApp', 'runway' ),
			'href'  => 'whatsapp://send?text=${title}%0A${url}',
			'icon'  => 'whatsapp',
			'color' => '#25D366',
		),
		'xing'        => array(
			'name'  => __( 'XING', 'runway' ),
			'text'  => __( 'Share on XING', 'runway' ),
			'href'  => 'https://www.xing.com/spi/shares/new?url=${url}',
			'icon'  => 'xing',
			'color' => '#005A60',
		),
	);

	// Output HTML markup.
	echo '<div class="social-share-links">';
	foreach ( $services as $service ) {

		$href = $services_data[ $service ]['href'];
		$icon = $services_data[ $service ]['icon'];
		$text = $services_data[ $service ]['text'];

		$href = str_replace( '${url}', $url, $href );
		$href = str_replace( '${title}', $title, $href );
		$href = str_replace( '${media}', $media, $href );

		?>
		<a class="social-share-links__item social-share-links__item--<?php echo esc_attr( $service ); ?>" href="<?php echo esc_url( $href ); ?>" target="_blank"><i class="socicon-<?php echo esc_attr( $icon ); ?> social-share-links__icon"></i><span class="screen-reader-text"><?php echo esc_html( $text ); ?></span></a>
		<?php
	}
	echo '</div>';
}

/**
 * Prints HTML for social share links.
 *
 * @param string $classes CSS classes, first class name will be BEM block.
 */
function runway_search_form( $classes = 'search-form' ) {

	// Get block class name.
	$block = explode( ' ', $classes )[0];

	if ( strpos( $block, '__' ) !== false ) {
		$block = $block . '-';
	} else {
		$block = $block . '__';
	}

	$label_classes  = $block . 'label';
	$input_classes  = $block . 'input';
	$submit_classes = $block . 'submit';

	?>
	<form role="search" method="get" class="<?php echo esc_attr( $classes ); ?>" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<label class="<?php echo esc_attr( $label_classes ); ?>">
			<span class="screen-reader-text"><?php echo esc_html_x( 'Search for:', 'label', 'runway' ); ?></span>
			<input type="search" class="<?php echo esc_attr( $input_classes ); ?>" placeholder="<?php echo esc_attr_x( 'Search', 'placeholder', 'runway' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
		</label>
		<button type="submit" class="<?php echo esc_attr( $submit_classes ); ?>"><i class="material-icons" aria-hidden="true">&#xE8B6;</i><span class="screen-reader-text"><?php echo esc_html_x( 'Search', 'submit button', 'runway' ); ?></span></button>
	</form>
	<?php
}
