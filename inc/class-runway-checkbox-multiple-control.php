<?php
/**
 * Multiple checkbox control.
 *
 * @package Runway
 */

/**
 * Class used to create a custom control.
 */
class Runway_Checkbox_Multiple_Control extends WP_Customize_Control {

	/**
	 * The type of customize control being rendered.
	 *
	 * @var string
	 */
	public $type = 'runway-checkbox-multiple';

	/**
	 * Displays the control content.
	 */
	public function render_content() {
		if ( empty( $this->choices ) ) {
			return;
		}
		?>
		<?php if ( ! empty( $this->label ) ) : ?>
		<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
		<?php endif; ?>
		<?php if ( ! empty( $this->description ) ) : ?>
		<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
		<?php endif; ?>
		<?php $checked = is_string( $this->value() ) ? explode( ',', $this->value() ) : $this->value(); ?>
		<ul>
			<?php foreach ( $this->choices as $value => $label ) : ?>
			<li>
				<label>
					<input type="checkbox" value="<?php echo esc_attr( $value ); ?>"<?php checked( in_array( $value, $checked, true ) ); ?> /><?php echo esc_html( $label ); ?>
				</label>
			</li>
			<?php endforeach; ?>
		</ul>
		<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( implode( ',', $checked ) ); ?>" />
		<?php
	}

	/**
	 * Loads control scripts and styles.
	 */
	public function enqueue() {
		wp_enqueue_style(
			'runway-customize-custom-controls-css',
			get_theme_file_uri( '/assets/css/customize-custom-controls.css' ),
			array(),
			null
		);
		wp_enqueue_script(
			'runway-customize-custom-controls-js',
			get_theme_file_uri( '/assets/js/customize-custom-controls.js' ),
			array( 'jquery' ),
			null,
			true
		);
	}
}
