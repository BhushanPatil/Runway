<?php
/**
 * Template part for displaying tags of a blog post
 *
 * @package Runway
 */

$tags = get_the_tags();
if ( $tags ) :
?>
	<div class="tags">
		<?php foreach ( $tags as $tag ) : ?>
		<a class="tags__item" href="<?php echo esc_url( get_tag_link( $tag->term_id ) ); ?>" rel="tag"><?php echo esc_html( $tag->name ); ?></a>
		<?php endforeach; ?>
	</div>
<?php
endif;
