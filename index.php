<?php
/**
 * The main template file
 *
 * @package Runway
 */

$show_sidebar            = get_theme_mod( 'homepage_show_sidebar', false );
$show_heading            = get_theme_mod( 'homepage_show_heading', false );
$latest_posts_heading    = get_theme_mod( 'homepage_latest_posts_heading', '' );
$latest_posts_subheading = get_theme_mod( 'homepage_latest_posts_subheading', '' );

$section_clases = 'section';
if ( $show_sidebar ) {
	$section_clases .= ' section--sidebar-' . get_theme_mod( 'homepage_sidebar_width', 'one-fourth' );
}

get_header();
?>
<div class="main">
	<div class="main__inner">
		<div class="<?php echo esc_attr( $section_clases ); ?>">
			<?php if ( $show_heading ) : ?>
			<div class="section__header">
				<?php if ( ! empty( $latest_posts_heading ) ) : ?>
				<p class="section__header-title">
					<span class="section__header-title-text"><?php echo esc_html( $latest_posts_heading ); ?></span>
				</p>
				<?php endif; ?>
				<?php if ( ! empty( $latest_posts_subheading ) ) : ?>
				<p class="section__header-sub-title">
					<span class="section__header-sub-title-text"><?php echo esc_html( $latest_posts_subheading ); ?></span>
				</p>
				<?php endif; ?>
			</div>
			<?php endif; ?>
			<div class="section__content">
				<main id="main" class="section__primary-area" role="main">
					<?php get_template_part( 'template-parts/home/layout', 'masonry' ); ?>
					<?php get_template_part( 'template-parts/pagination/pagination' ); ?>
				</main>
				<?php if ( $show_sidebar ) : ?>
				<aside class="section__secondary-area" role="complementary">
					<?php dynamic_sidebar( 'sidebar' ); ?>
				</aside>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
