<?php
/**
 * Image select control.
 *
 * @package Runway
 */

/**
 * Class used to create a custom control.
 */
class Runway_Image_Select_Control extends WP_Customize_Control {

	/**
	 * Image select columns.
	 *
	 * @var int
	 */
	public $columns = 3;

	/**
	 * The type of customize control being rendered.
	 *
	 * @var   string
	 */
	public $type = 'runway-image-select';

	/**
	 * Displays the control content.
	 */
	public function render_content() {
		if ( empty( $this->choices ) ) {
			return;
		}
		?>
		<?php if ( ! empty( $this->label ) ) : ?>
		<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
		<?php endif; ?>
		<?php if ( ! empty( $this->description ) ) : ?>
		<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
		<?php endif; ?>
		<ul class="runway-image-select-columns-<?php echo esc_attr( $this->columns ); ?>">
		<?php foreach ( $this->choices as $value => $args ) : ?>
			<li>
				<?php
				$input_id   = "{$this->id}-{$value}";
				$input_name = "_customize-radio-{$this->id}";
				?>
				<input type="radio" value="<?php echo esc_attr( $value ); ?>" name="<?php echo esc_attr( $input_name ); ?>" id="<?php echo esc_attr( $input_id ); ?>" <?php $this->link(); ?> <?php checked( $this->value(), $value ); ?> />
				<label for="<?php echo esc_attr( $input_id ); ?>">
					<?php
					$width = false;
					if ( isset( $args['width'] ) ) {
						$width = esc_attr( $args['width'] );
					}
					$height = false;
					if ( isset( $args['height'] ) ) {
						$height = esc_attr( $args['height'] );
					}
					?>
					<img src="<?php echo esc_url( $args['url'] ); ?>" alt="<?php echo esc_attr( $args['label'] ); ?>"<?php echo ( $width ? ' width="' . $width . '"' : '' ); // WPSC: XSS ok. ?><?php echo ( $height ? ' height="' . $height . '"' : '' ); // WPSC: XSS ok. ?> />
					<span class="screen-reader-text1"><?php echo esc_html( $args['label'] ); ?></span>
				</label>
			</li>
		<?php endforeach; ?>
		</ul>
		<?php
	}

	/**
	 * Loads control scripts and styles.
	 */
	public function enqueue() {
		wp_enqueue_style(
			'runway-customize-custom-controls-css',
			get_theme_file_uri( '/assets/css/customize-custom-controls.css' ),
			array(),
			null
		);
	}
}
