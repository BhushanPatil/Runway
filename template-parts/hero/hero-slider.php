<?php
/**
 * Template part for displaying hero section.
 *
 * @package Runway
 */

$feat_query = new WP_Query( array(
	'showposts'  => get_theme_mod( 'hero_slider_number_of_posts', 5 ),
	'meta_query' => array(
		array(
			'key' => '_thumbnail_id',
		),
	),
) );

if ( $feat_query->have_posts() ) :

	$classes = 'slick-slider';

	$style    = get_theme_mod( 'hero_slider_style', 'multiple-items' );
	$autoplay = get_theme_mod( 'hero_slider_autoplay', false );
	$arrows   = get_theme_mod( 'hero_slider_arrows', true );
	$dots     = get_theme_mod( 'hero_slider_dots', true );
	$fade     = get_theme_mod( 'hero_slider_fade', true );
	$infinite = get_theme_mod( 'hero_slider_infinite', true );

	$extra_settings = '';
	if ( 'multiple-items' === $style ) {
		$classes       .= ' slick-slide-spacing slick-slider-with-arrows-on-slide';
		$arrows         = false;
		$fade           = false;
		$extra_settings = ',
		"centerMode": true,
		"centerPadding": "0%",
		"variableWidth": true,
		"responsive": [
			{
				"breakpoint": "840",
				"settings": {
					"centerMode": false,
					"variableWidth": false
				}
			}
		]';
	}
	$settings = '{
		"autoplay": ' . ( $autoplay ? 'true' : 'false' ) . ',
		"arrows": ' . ( $arrows ? 'true' : 'false' ) . ',
		"dots": ' . ( $dots ? 'true' : 'false' ) . ',
		"fade": ' . ( $fade ? 'true' : 'false' ) . ',
		"infinite": ' . ( $infinite ? 'true' : 'false' ) . '
		' . $extra_settings . '
	}';

	?>
	<div class="<?php echo esc_attr( $classes ); ?>" data-slick="<?php echo esc_attr( $settings ); ?>">
		<?php while ( $feat_query->have_posts() ) : ?>
		<?php $feat_query->the_post(); ?>
		<?php
		$image            = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full-thumb' );
		$category         = get_the_category();
		$permalink        = get_permalink();
		$author_id        = get_the_author_meta( 'ID' );
		$author_name      = get_the_author();
		$author_posts_url = get_author_posts_url( $author_id );
		$date             = get_the_date( get_option( 'date_format' ) );
		$date_w3c         = get_the_date( DATE_W3C );
		?>
		<article>
			<div class="slick-slide-content">
				<div class="hero__slide">
					<img class="hero__slide-cover-img" src="<?php echo esc_url( $image[0] ); ?>" style="background-image:url(<?php echo esc_url( $image[0] ); ?>);" />
					<div class="hero__slide-content">
						<h2 class="hero__slide-title">
							<?php the_title( sprintf( '<a class="hero__slide-title-link" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>' ); ?>
						</h2>
						<div class="hero__slide-metadata">
							<span class="hero__slide-datetime">
								<a class="hero__slide-datetime-link" href="<?php echo esc_url( $permalink ); ?>">
									<time class="hero__slide-datetime-tag" datetime="<?php echo esc_attr( $date_w3c ); ?>"><?php echo esc_html( $date ); ?></time>
								</a>
							</span>
							<span class="hero__slide-category">
								<a class="hero__slide-category-link" href="<?php echo esc_url( get_category_link( $category[0]->cat_ID ) ); ?>"><?php echo esc_html( $category[0]->cat_name ); ?></a>
							</span>
						</div>
						<a class="hero__slide-cta button button--text-primary-dark button--ghost"href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html__( 'View Post', 'runway' ); ?><i class="material-icons">&#xE315;</i></a>
					</div>
				</div>
			</div>
		</article>
		<?php
		endwhile;
		wp_reset_postdata();
		?>
	</div>
<?php
endif;
