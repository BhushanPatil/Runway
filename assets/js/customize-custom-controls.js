( function( $ ) {
	$( document ).ready( function() {

		$( '.customize-control-runway-checkbox-multiple input[type="checkbox"]' ).on( 'change', function() {

			var input = $( this ).closest( '.customize-control' ).find( 'input[type="hidden"]' ),
				checked = [];

			if ( input.val() ) {
				checked = input.val().split( ',' );
			}

			if ( this.checked && checked.indexOf( this.value ) ) {
				checked.push( this.value );
			} else {
				checked.splice( checked.indexOf( this.value ), 1 );
			}
			input.val( checked.join( ',' ) ).trigger( 'change' );

		});

		// Use buttonset() for radio images.
		$( '.customize-control-runway-image-select .buttonset' ).buttonset();

		// Handles setting the new value in the customizer.
		$( '.customize-control-runway-image-select input:radio' ).change(
			function() {

				// Get the name of the setting.
				var setting = $( this ).attr( 'data-customize-setting-link' );

				// Get the value of the currently-checked radio input.
				var image = $( this ).val();

				// Set the new value.
				wp.customize( setting, function( obj ) {
					obj.set( image );
				});
			}
		);

	});

} ( jQuery ) );
