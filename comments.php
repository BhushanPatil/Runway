<?php
/**
 * The template for displaying comments
 *
 * @package Runway
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<div class="comments-area">
	<?php if ( have_comments() ) : ?>
	<h2 class="comments-area__title">
		<?php
		$comments_number = get_comments_number();
		if ( '1' === $comments_number ) {
			esc_html_e( 'One comment', 'runway' );
		} else {
			echo esc_html(
				sprintf(
					/* translators: %s: Number of comments */
					_n(
						'%s comment',
						'%s comments',
						$comments_number,
						'runway'
					),
					number_format_i18n( $comments_number )
				)
			);
		}
		?>
	</h2>
	<ol class="comments-list">
		<?php
		wp_list_comments( array(
			'walker'      => new Runway_Walker_Comment( 'comments-list', 'comment' ),
			'max_depth'   => 2,
			'style'       => 'ol',
			'avatar_size' => 32,
		));
		?>
	</ol>
	<?php
	the_comments_pagination( array(
		'prev_text' => __( 'Previous', 'runway' ),
		'next_text' => __( 'Next', 'runway' ),
	) );
	endif;
	?>
	<?php
	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
	<p class="comments-area__no-comments-note"><?php esc_html_e( 'Comments are closed.', 'runway' ); ?></p>
	<?php endif; ?>
	<?php
	$commenter     = wp_get_current_commenter();
	$user          = wp_get_current_user();
	$user_identity = $user->exists() ? $user->display_name : '';
	$req           = get_option( 'require_name_email' );
	$label         = $req ? '&nbsp;<span class="comments-respond__required-field">*</span>' : '';
	$aria_req      = $req ? " aria-required='true'" : '';
	$html_req      = $req ? " required='required'" : '';

	$fields = array(
		'author' => '<p class="comments-respond__field">' .
					'<label class="comments-respond__field-label" for="author">' . __( 'Name', 'runway' ) . $label . '</label>' .
					'<input class="comments-respond__field-input" id="author" name="author" type="text" placeholder="' . esc_attr__( 'Name', 'runway' ) . ( $req ? ' *' : '' ) . '" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $html_req . $aria_req . ' title="' . esc_attr__( 'Your name', 'runway' ) . '" />' .
					'</p>',
		'email'  => '<p class="comments-respond__field">' .
					'<label class="comments-respond__field-label" for="email">' . __( 'Email', 'runway' ) . $label . '</label>' .
					'<input class="comments-respond__field-input" id="email" name="email" type="email" placeholder="' . esc_attr__( 'Email', 'runway' ) . ( $req ? ' *' : '' ) . '" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30"' . $html_req . $aria_req . ' title="' . esc_attr__( 'Your email address', 'runway' ) . '" />' .
					'</p>',
		'url'    => '<p class="comments-respond__field">' .
					'<label class="comments-respond__field-label" for="url">' . __( 'Website', 'runway' ) . '</label>' .
					'<input class="comments-respond__field-input" id="url" name="url" type="url"  placeholder="' . esc_attr__( 'Website', 'runway' ) . '" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" title="' . esc_attr__( 'Your website', 'runway' ) . '" />' .
					'</p>',
	);

	if ( get_theme_mod( 'comments_hide_url_field', true ) ) {
		unset( $fields['url'] );
	}

	$comment_form_args = array(
		'fields'               => apply_filters( 'comment_form_default_fields', $fields ),
		'comment_field'        => '<p class="comments-respond__comment">' .
								'<label class="comments-respond__comment-label" for="comment">' . esc_html__( 'Comment', 'runway' ) . '</label>' .
								'<textarea class="comments-respond__comment-input" id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required" placeholder="' . esc_attr__( 'Comment', 'runway' ) . '" title="' . esc_attr__( 'Your comment', 'runway' ) . '"></textarea>' .
								'</p>',
		'must_log_in'          => '<p class="comments-respond__must-log-in">' . sprintf(
			/* translators: %s: login URL */
			__( 'You must be <a href="%s">logged in</a> to post a comment.', 'runway' ),
			wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
		) . '</p>',
		'logged_in_as'         => '<p class="comments-respond__logged-in-as">' . sprintf(
			/* translators: 1: edit user link, 2: accessibility text, 3: user name, 4: logout URL */
			__( 'Logged in as <a class="comments-respond__logged-in-as-link" href="%1$s" aria-label="%2$s">%3$s</a>. <a class="comments-respond__logged-out-link" href="%4$s">Log out?</a>', 'runway' ),
			get_edit_user_link(),
			/* translators: %s: user name */
			esc_attr( sprintf( __( 'Logged in as %s. Edit your profile.', 'runway' ), $user_identity ) ),
			$user_identity,
			wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) )
		) . '</p>',
		'comment_notes_before' => '',
		'class_form'           => 'comments-respond__form',
		'class_submit'         => 'comments-respond__submit-button',
		'title_reply_before'   => '<h3 class="comments-respond__title">',
		'title_reply_after'    => '</h3>',
		'submit_field'         => '<p class="comments-respond__submit">%1$s %2$s</p>',
	);

	add_action( 'comment_form_before', function () {
		$classes = 'comments-respond';
		if ( get_theme_mod( 'comments_hide_labels', true ) ) {
			$classes .= ' comments-respond--hide-labels';
		}
		echo '<div class="' . $classes . '">'; // WPCS: xss ok.
	});
	add_action( 'comment_form_after', function () {
		echo '</div>';
	});

	add_action( 'comment_form_before_fields', function () {
		echo '<div class="comments-respond__fields">';
	});
	add_action( 'comment_form_after_fields', function () {
		echo '</div>';
	});

	comment_form( $comment_form_args );
	?>
</div>
