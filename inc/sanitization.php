<?php
/**
 * Input data sanitization functions and definitions
 *
 * @package Runway
 */

/**
 * Textarea:        wp_filter_nohtml_kses // Removes all HTML from content
 * Email:           sanitize_email        // Removes all invalid characters
 * URL:             esc_url_raw           // Cleans URL from all invalid characters
 * Number:          absint                // Converts value to a non-negative integer
 * Drop-down Pages: absint                // Input value is a page ID so it must be a positive integer
 * Color Code:      sanitize_hex_color    // Validates 3 or 6 digit HTML hex color code
 * CSS Code:        wp_strip_all_tags     // Strip all HTML tags including script and style
 * HTML Code:       wp_kses_post          // Keeps only HTML tags that are allowed in post content
 *
 * List of WordPress Sanitization Functions:
 *
 * absint                               Converts value to positive integer, useful for numbers, IDs, etc.
 * esc_url_raw                          For inserting URL in database safely
 * sanitize_email                       Strips out all characters that are not allowable in an email address
 * sanitize_file_name                   Removes special characters that are illegal in filenames on certain operating system
 * sanitize_hex_color                   Returns 3 or 6 digit hex color with #, or nothing
 * sanitize_hex_color_no_hash           The same as above but without a #
 * sanitize_html_class                  Sanitizes an HTML classname to ensure it only contains valid characters
 * sanitize_key                         Lowercase alphanumeric characters, dashes and underscores are allowed
 * sanitize_mime_type                   Useful to save mime type in DB, e.g. uploaded file's type
 * sanitize_option                      Sanitizes values like update_option() and add_option() does for various option types.
 * sanitize_sql_orderby                 Ensures a string is a valid SQL order by clause
 * sanitize_text_field                  Removes all HTML markup, as well as extra whitespace, leaves nothing but plain text
 * sanitize_title                       Returned value intented to be suitable for use in a URL
 * sanitize_title_for_query             Used for querying the database for a value from URL
 * sanitize_title_with_dashes           Same as above but it does not replace special accented characters
 * sanitize_user                        Sanitize username stripping out unsafe characters
 * wp_filter_post_kses, wp_kses_post    It keeps only HTML tags which are allowed in post content as well
 * wp_kses                              Allows only HTML tags and attributes that you specify
 * wp_kses_data                         Sanitize content with allowed HTML Kses rules
 * wp_rel_nofollow                      Adds rel nofollow string to all HTML A elements in content
 *
 *
 * Some PHP functions to fill some gaps:
 *
 * filter_var( $variable, $filter )     Filters a variable with a specific filter
 * strlen()                             Gets string length, useful for ZIP codes, phone numbers
 */

/**
 * Checkbox sanitization function.
 *
 * @param bool|int|string $input Input data.
 */
function runway_sanitize_checkbox( $input ) {
	// Returns true if checkbox is checked.
	return ( $input ? true : false );
}

/**
 * Radio box sanitization function.
 *
 * @param bool|int|string $input Input data.
 * @param object          $setting Customizer setting data to get radio choices.
 */
function runway_sanitize_radio( $input, $setting ) {

	// Input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only.
	$input = sanitize_key( $input );

	// Get the list of possible radio box options.
	$choices = $setting->manager->get_control( $setting->id )->choices;

	// Return input if valid or return default option.
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

/**
 * Select sanitization function.
 *
 * @param bool|int|string $input Input data.
 * @param object          $setting Customizer setting data to get radio choices.
 */
function runway_sanitize_select( $input, $setting ) {

	// Input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only.
	$input = sanitize_key( $input );

	// Get the list of possible select options.
	$choices = $setting->manager->get_control( $setting->id )->choices;

	// Return input if valid or return default option.
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );

}

/**
 * File input sanitization function.
 *
 * @param string $file File name or path.
 * @param object $setting Customizer setting data.
 */
function runway_sanitize_file( $file, $setting ) {

	// Allowed file types.
	$mimes = array(
		'jpg|jpeg|jpe' => 'image/jpeg',
		'gif'          => 'image/gif',
		'png'          => 'image/png',
	);

	// Check file type from file name.
	$file_ext = wp_check_filetype( $file, $mimes );

	// If file has a valid mime type return it, otherwise return default.
	return ( $file_ext['ext'] ? $file : $setting->default );
}

/**
 * Script input sanitization function.
 *
 * @param string $input Input data.
 */
function runway_sanitize_js_code( $input ) {
	return base64_encode( $input );
}

/**
 * Output escape function.
 *
 * @param string $input Input data.
 */
function runway_escape_js_output( $input ) {
	return esc_textarea( base64_decode( $input ) );
}

/**
 * CSS opacity value sanitization function.
 *
 * @param bool|int|string $input Input data.
 * @param object          $setting Customizer setting data.
 */
function runway_sanitize_opacity( $input, $setting ) {

	// Get default value.
	$default = $setting->manager->get_setting( $setting->id )->default;

	// Return input if valid else return default value.
	return ( 0 <= $input && 1 >= $input ? $input : $default );
}

/**
 * Float value sanitization function.
 *
 * @param bool|int|string $input Input data.
 */
function runway_sanitize_float( $input ) {
	return filter_var(
		$input,
		FILTER_SANITIZE_NUMBER_FLOAT,
		FILTER_FLAG_ALLOW_FRACTION
	);
}
