<?php
/**
 * Template part for displaying hero section.
 *
 * @package Runway
 */

?>
<div class="hero__intro">
	<h1 class="hero__intro-title">
		<span class="hero__intro-title-text"><?php echo esc_html( get_theme_mod( 'hero_intro_title', get_bloginfo( 'name' ) ) ); ?></span>
	</h1>
	<h4 class="hero__intro-copy">
		<span class="hero__intro-copy-text"><?php echo wp_kses_post( get_theme_mod( 'hero_intro_copy', get_bloginfo( 'description', 'display' ) ) ); ?></span>
	</h4>
</div>
