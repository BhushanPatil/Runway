<?php
/**
 * Template part for displaying numeric pagination.
 *
 * @package Runway
 */

/* translators: 1: Previous page arrow */
$prev_text = sprintf( esc_html__( '%s Previous', 'runway' ), '<i class="material-icons">&#xE5C4;</i>' );

/* translators: 1: Next page arrow */
$next_text = sprintf( esc_html__( 'Next %s', 'runway' ), '<i class="material-icons">&#xE5C8;</i>' );

the_posts_pagination( array(
	'mid_size'  => 2,
	'prev_text' => $prev_text,
	'next_text' => $next_text,
) );
